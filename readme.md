Football Matcher - Useful links & Info
================

# Log in URL to REST API session (bypass oauth2)
```
http://login.footballmatcher.co.uk
```

# Getting Oauth2 Access Token 
```
POST REST TO: http://footballmatcher.co.uk:8000/o/token/
client_id: T;Ousuhi_L!DVFbpWD;hjuDi!QXpsW4aR0lwb!ID
client_secret: @_.vIRKmdjCjGNCdp51j4ixTTz;r;Q-UOcRaLrfN6Vrvn9KlWCq;pCZPwIRv=zcO166;nD-DZY?KjoBqmnVBWzMl.keQa.hIf@;c6vYXhFyZtwtf.4bQM3Ls@RwBkK.a
grant_type: password
username: fm_dev
password: fm_dev
```

# Using Oauth2 Access Token 
```
For every secure API call add the following to the header: 
Authorization: Bearer loAVtHctELdYAv6IeR49I2W3lp00Dq
```

# Other Userfull URLs
```
API Documentation      - http://docs.footballmatcher.co.uk
Continuous Integration - http://jenkins.footballmatcher.co.uk
Communication Tool     - http://slack.footballmatcher.co.uk
Agile Planning         - http://agile.footballmatcher.co.uk
```

# Recommended Software
```
postman  (chrome extension for REST calls)
pycharm  (IDE for python & django development)
webstorm (IDE for webdevelopment)
pgAdmin  (GUI for postgres)
```

Football Matcher - Setting up the Linux environment
================

# Install useful tools

```
sudo apt-get install vim git sublime-text 
```

# Install python stuff

```
sudo apt-get install python-pip virtualenvwrapper python-dev
```

# Install Postgresql stuff

```
sudo apt-get install postgresql postgresql-contrib postgresql-client libpq-dev postgresql-server-dev-all pgadmin3 
```

# Install RabbitMQ Task Server

```
wget https://www.rabbitmq.com/rabbitmq-signing-key-public.asc
sudo apt-key add rabbitmq-signing-key-public.asc
sudo apt-get install rabbitmq-server
```

# Steps to setup db

##Log on to pgadmin and setup this database

user: fm_dev
password: fm_dev
db_name:footballmatcher
port: 5432

## Import dummy data into postgresql database
Use pgadmin3 to import the dummy data into footballmatcher database from the scripts in /footballmatcher/database


# Set up SSH keys to Bit bucket by following instructions here
https://confluence.atlassian.com/pages/viewpage.action?pageId=270827678

# Clone football matcher repo 
```
git clone git@bitbucket.org:footballmatcher/footballmatcher.git
```

# Then create new virtualenv
```
mkvritualenv fm 
```

# Then activate fm virtualenv
```
workon fm
```
# Then install python modules using pip
```
pip install -r requirements.txt 
```
# Start Celery Task queue
```
cd footballmatcher/footballmatcher 
celery -A footballmatcher worker -l info
```
# Create superuser 
```
python manage.py createsuperuser fm_dev
```
# Runserver
```
python manage.py runserver
```