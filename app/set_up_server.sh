#!/bin/bash

# Run as root on Ubuntu

# Set up your locales
#dpkg-reconfigure locales

#Create the file /etc/apt/sources.list.d/pgdg.list, and add a line for the repository
#deb http://apt.postgresql.org/pub/repos/apt/ trusty-pgdg main
#Import the repository signing key, and update the package lists
#wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | \
#  sudo apt-key add -
#sudo apt-get update

apt-get install postgresql-9.4 postgresql-client-9.4 postgresql-contrib-9.4 libpq-dev postgresql-server-dev-9.4;


# Add  Rabbit MQ to apt-get DB
echo "Installing required software for FM"
echo 'deb http://www.rabbitmq.com/debian/ testing main' >> /etc/apt/sources.list;
wget https://www.rabbitmq.com/rabbitmq-signing-key-public.asc;
apt-key add rabbitmq-signing-key-public.asc;

# Update apt-get
apt-get update;

# Install software
apt-get -y install vim;
apt-get -y install libjemalloc-dev
apt-get -y install gcc;
apt-get -y install git;
apt-get -y install python-pip;
apt-get -y install python-dev;
apt-get -y install rabbitmq-server;

# Install Redis
wget http://download.redis.io/redis-stable.tar.gz;
tar xvzf redis-stable.tar.gz;
cd redis-stable;
make;
make install;

# Install VirtualEnv Wrapper
echo "Setup virtual environment for python"
pip install virtualenvwrapper;

# Append to bashrc
export WORKON_HOME=~/Envs;
mkdir -p $WORKON_HOME;
source /usr/local/bin/virtualenvwrapper.sh;

# Add to bashrc
echo 'source /usr/local/bin/virtualenvwrapper.sh;' >> ~/.bashrc;

# Add FM User
echo "Please enter username"
read input_username;
adduser $input_username sudo;
su fm;
cd ~/;

# Create ssh keys for bit bucket
echo "Set up SSH keys for bit bucket"
ssh-keygen;
ssh-agent /bin/bash;
ssh-add ~/.ssh/id_rsa;
ssh-add -l;
echo "Go to footballmatcher on bitbucket and add the following key"
cat ~/.ssh/id_rsa.pub;
echo "Press a key to continue"
read input_dontcare;

# Check out code from Bitbucket
echo "Downloading code from bitbucket"
git clone git@bitbucket.org:footballmatcher/footballmatcher.git;

# Append to bashrc in fm_dev home
export WORKON_HOME=~/Envs;
mkdir -p $WORKON_HOME;
source /usr/local/bin/virtualenvwrapper.sh;

# Add to bashrc
echo 'source /usr/local/bin/virtualenvwrapper.sh;' >> ~/.bashrc;


# Make virtual environment
mkvirtualenv fm;

# Active env
workon fm;

# Install requirements for python
echo "Installing requirements for python"
cd footballmatcher;
pip install -r requirements.txt;

# Set up database as root
#edit file
vim /etc/postgresql/9.4/main/postgresql.conf;

# uncomment the following:
# listen_addresses = 'localhost'

# alter this file:
vim /etc/postgresql/9.4/main/pg_hba.conf
# local   all         postgres                          md5

# then restart server
service postgresql restart

sudo -u postgres psql postgres
\password postgres

# then escape (clt-d) back to root
psql -c 'create database footballmatcher;' -U postgres; 
psql -c "CREATE USER fm_dev WITH SUPERUSER PASSWORD 'fm_dev';" -U postgres

# Set up database
su fm_dev;
cd footballmatcher/footballmatcher;
python manage.py migrate;

# restore from tar file
cd ../database;
pg_restore -c -i -U postgres -d footballmatcher -v "dummy_data_8th_April_2015_tar" -W

# migrate
cd ../footballmatcher;
python manage.py migrate;

# then runserver
python manage.py runserver 0.0.0.0:8000









 


