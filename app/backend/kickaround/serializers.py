from rest_framework import serializers
from .models import *
from .validators import validate_email_unique

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('pk', 'first_name', 'last_name', 'username')

class LocationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Location
        fields = ('pk',
                  'sports_centre',
                  'town_city',
                  'postcode',
                  'county',
                  'country')

class KickAroundSerializer(serializers.ModelSerializer):
    kickaround_sides = serializers.StringRelatedField(many=True)
    kickaround_members = serializers.StringRelatedField(many=True)

    class Meta:
        model = KickAround
        fields = ('pk',
                  'name',
                  'avatar',
                  'description',
                  'standard',
                  'kickaround_members',
                  'kickaround_sides',
                  'location',
                  'day_of_week',
                  'weekly_time',
                  'pitch_hire_cost',
                  'average_cost',
                  'ka_privacy')

class KickAroundMemberSerializer(serializers.ModelSerializer):
    user = UserSerializer(read_only=True)
    class Meta:
        model = KickAroundMember
        fields = ('pk',
                  'user',
                  'date_joined',
                  'role',
                  'member_status')

class KickAroundGameSerializer(serializers.ModelSerializer):

    class Meta:
        model = KickAroundGame
        fields = ('pk',
                  'location',
                  'start_time',
                  'duration',
                  'pitch_hire_cost',
                  'game_status',
                  'game_members')

class KickAroundGameMemberSerializer(serializers.ModelSerializer):
    kickaround_member = KickAroundMemberSerializer(read_only=True)
    class Meta:
        model = KickAroundGameMember
        fields = ('pk',
                  'date_joined',
                  'kickaround_member',
                  'team',
                  'role',
                  'status')

class KickAroundPaymentSerializer(serializers.ModelSerializer):
    class Meta:
        model = KickAroundPayment
        fields = ('pk',
                  'payee',
                  'payer',
                  'payment_status',
                  'payment_type',
                  'amount',
                  'currency')

class KickAroundGameKitchenSinkSerializer(serializers.ModelSerializer):
    kickaround_game_members = KickAroundGameMemberSerializer(many=True, read_only=True)
    location = LocationSerializer(read_only=True)
    class Meta:

        model = KickAroundGame
        fields = ('pk',
                  'location',
                  'start_time',
                  'duration',
                  'pitch_hire_cost',
                  'game_status',
                  'kickaround_game_members')

class KickAroundKitchenSinkSerializer(serializers.ModelSerializer):
    kickaround_members = KickAroundMemberSerializer(many=True, read_only=True)
    kickaround_games = KickAroundGameKitchenSinkSerializer(many=True, read_only=True)
    location = LocationSerializer(read_only=True)

    class Meta:
        model = KickAround
        fields = ('pk',
                  'name',
                  'avatar',
                  'description',
                  'standard',
                  'day_of_week',
                  'kickaround_members',
                  'kickaround_games',
                  'location',
                  'weekly_time',
                  'pitch_hire_cost',
                  'average_cost',
                  'ka_privacy')


class UserSerializer(serializers.ModelSerializer):
    # Validate that email address is unique
    email = serializers.EmailField(label = "Email", required=True, validators=[validate_email_unique])

    class Meta:
        model = User
        fields = ('id', 'username', 'first_name', 'last_name','email','password')