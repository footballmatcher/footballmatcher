# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('kickaround', '0008_auto_20150305_2357'),
    ]

    operations = [
        migrations.AddField(
            model_name='kickaroundgame',
            name='cancellation_reason',
            field=models.CharField(max_length=500, null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='kickaroundgame',
            name='cost_per_player',
            field=models.FloatField(null=True),
            preserve_default=True,
        ),
    ]
