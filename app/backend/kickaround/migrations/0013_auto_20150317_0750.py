# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('kickaround', '0012_auto_20150317_0748'),
    ]

    operations = [
        migrations.CreateModel(
            name='KickAroundPayment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('payment_status', models.CharField(max_length=100, choices=[(b'REQUESTED', b'Requested'), (b'PAID', b'Paid'), (b'OUTSTANDING', b'Outstanding')])),
                ('payment_type', models.CharField(max_length=100, choices=[(b'CASH', b'Cash'), (b'PAYPAL', b'PayPal')])),
                ('amount', models.DecimalField(max_digits=19, decimal_places=2)),
                ('currency', models.CharField(max_length=20, choices=[(b'USD', b'US Dollar(s)'), (b'GBP', b'British Pound(s)')])),
                ('kickaround', models.ForeignKey(related_name='payments', to='kickaround.KickAround', null=True)),
                ('kickaround_game', models.ForeignKey(related_name='game_payments', to='kickaround.KickAroundGame', null=True)),
                ('payee', models.ForeignKey(related_name='payee', to=settings.AUTH_USER_MODEL)),
                ('payer', models.ForeignKey(related_name='payer', to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.RemoveField(
            model_name='kickaroundgamepayment',
            name='kickaround',
        ),
        migrations.RemoveField(
            model_name='kickaroundgamepayment',
            name='kickaround_game',
        ),
        migrations.RemoveField(
            model_name='kickaroundgamepayment',
            name='payee',
        ),
        migrations.RemoveField(
            model_name='kickaroundgamepayment',
            name='payer',
        ),
        migrations.DeleteModel(
            name='KickAroundGamePayment',
        ),
    ]
