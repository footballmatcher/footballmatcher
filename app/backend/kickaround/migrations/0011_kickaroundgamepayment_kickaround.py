# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('kickaround', '0010_kickaroundgamepayment_kickaround_game'),
    ]

    operations = [
        migrations.AddField(
            model_name='kickaroundgamepayment',
            name='kickaround',
            field=models.ForeignKey(related_name='kickaround_payments', to='kickaround.KickAround', null=True),
            preserve_default=True,
        ),
    ]
