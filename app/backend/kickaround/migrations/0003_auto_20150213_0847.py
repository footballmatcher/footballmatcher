# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('kickaround', '0002_auto_20150213_0846'),
    ]

    operations = [
        migrations.AlterField(
            model_name='kickaround',
            name='location',
            field=models.ForeignKey(to='kickaround.Location', null=True),
            preserve_default=True,
        ),
    ]
