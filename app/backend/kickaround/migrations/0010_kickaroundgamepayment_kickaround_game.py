# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('kickaround', '0009_auto_20150316_0745'),
    ]

    operations = [
        migrations.AddField(
            model_name='kickaroundgamepayment',
            name='kickaround_game',
            field=models.ForeignKey(related_name='kickaround_game_payments', to='kickaround.KickAroundGame', null=True),
            preserve_default=True,
        ),
    ]
