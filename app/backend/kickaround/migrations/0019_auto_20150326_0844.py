# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('kickaround', '0018_auto_20150325_0814'),
    ]

    operations = [
        migrations.AlterField(
            model_name='kickaroundside',
            name='side_name',
            field=models.CharField(max_length=100, null=True),
            preserve_default=True,
        ),
    ]
