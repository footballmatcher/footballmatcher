# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('kickaround', '0014_auto_20150317_0800'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='kickaroundpayment',
            name='kickaround',
        ),
        migrations.AlterField(
            model_name='kickaroundpayment',
            name='payee',
            field=models.ForeignKey(related_name='payee', to='kickaround.KickAroundGameMember'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='kickaroundpayment',
            name='payer',
            field=models.ForeignKey(related_name='payer', to='kickaround.KickAroundGameMember'),
            preserve_default=True,
        ),
    ]
