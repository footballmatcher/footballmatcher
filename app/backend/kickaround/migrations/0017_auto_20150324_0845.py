# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('kickaround', '0016_auto_20150319_0807'),
    ]

    operations = [
        migrations.AlterField(
            model_name='kickaround',
            name='ka_privacy',
            field=models.CharField(default=1, max_length=100, choices=[(b'2', b'closed'), (b'0', b'open'), (b'1', b'semi_open')]),
            preserve_default=True,
        ),
    ]
