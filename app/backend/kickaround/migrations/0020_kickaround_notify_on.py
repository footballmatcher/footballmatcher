# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('kickaround', '0019_auto_20150326_0844'),
    ]

    operations = [
        migrations.AddField(
            model_name='kickaround',
            name='notify_on',
            field=models.DateTimeField(null=True),
            preserve_default=True,
        ),
    ]
