# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('kickaround', '0003_auto_20150213_0847'),
    ]

    operations = [
        migrations.AlterField(
            model_name='kickaroundgame',
            name='kickaround',
            field=models.ForeignKey(related_name='kickaround_games', to='kickaround.KickAround'),
            preserve_default=True,
        ),
    ]
