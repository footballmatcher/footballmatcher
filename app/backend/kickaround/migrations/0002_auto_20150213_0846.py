# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('kickaround', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='kickaroundgame',
            name='duration',
            field=models.FloatField(default=1.0, null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='kickaroundgame',
            name='location',
            field=models.ForeignKey(to='kickaround.Location', null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='kickaroundgame',
            name='pitch_hire_cost',
            field=models.FloatField(null=True),
            preserve_default=True,
        ),
    ]
