# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('kickaround', '0015_auto_20150317_1844'),
    ]

    operations = [
        migrations.AddField(
            model_name='kickaroundpayment',
            name='kickaround',
            field=models.ForeignKey(related_name='payments', to='kickaround.KickAround', null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='kickaroundpayment',
            name='payee',
            field=models.ForeignKey(related_name='payee', to=settings.AUTH_USER_MODEL),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='kickaroundpayment',
            name='payer',
            field=models.ForeignKey(related_name='payer', to=settings.AUTH_USER_MODEL),
            preserve_default=True,
        ),
    ]
