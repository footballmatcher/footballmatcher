# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('kickaround', '0011_kickaroundgamepayment_kickaround'),
    ]

    operations = [
        migrations.AlterField(
            model_name='kickaroundgamepayment',
            name='kickaround',
            field=models.ForeignKey(related_name='payments', to='kickaround.KickAround', null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='kickaroundgamepayment',
            name='kickaround_game',
            field=models.ForeignKey(related_name='game_payments', to='kickaround.KickAroundGame', null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='kickaroundgamepayment',
            name='payee',
            field=models.ForeignKey(related_name='payee', to=settings.AUTH_USER_MODEL),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='kickaroundgamepayment',
            name='payer',
            field=models.ForeignKey(related_name='payer', to=settings.AUTH_USER_MODEL),
            preserve_default=True,
        ),
    ]
