# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('kickaround', '0005_auto_20150219_1228'),
    ]

    operations = [
        migrations.AddField(
            model_name='kickaround',
            name='automate_chase_payment',
            field=models.BooleanField(default=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='kickaround',
            name='automate_game_cancellation',
            field=models.BooleanField(default=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='kickaround',
            name='automate_game_confirmation',
            field=models.BooleanField(default=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='kickaround',
            name='automate_send_invites',
            field=models.BooleanField(default=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='kickaround',
            name='chase_payments_after',
            field=models.IntegerField(default=2),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='kickaround',
            name='chase_payments_on',
            field=models.DateTimeField(null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='kickaround',
            name='enforce_max_num_players',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='kickaround',
            name='max_num_players',
            field=models.IntegerField(null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='kickaround',
            name='min_num_players',
            field=models.IntegerField(null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='kickaround',
            name='next_game_on',
            field=models.DateTimeField(null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='kickaround',
            name='notify_hours_b4',
            field=models.IntegerField(default=4, null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='kickaround',
            name='notify_org_too_few_players',
            field=models.BooleanField(default=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='kickaround',
            name='num_of_teams',
            field=models.IntegerField(default=2, null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='kickaround',
            name='num_players_per_team',
            field=models.IntegerField(default=7),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='kickaround',
            name='send_invites_days_b4',
            field=models.IntegerField(default=2),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='kickaround',
            name='send_invites_on',
            field=models.DateTimeField(null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='kickaround',
            name='average_cost',
            field=models.FloatField(null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='kickaround',
            name='description',
            field=models.CharField(max_length=500, null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='kickaround',
            name='pitch_hire_cost',
            field=models.FloatField(null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='kickaroundgamemember',
            name='status',
            field=models.CharField(default=(b'REQUESTED', b'Requested'), max_length=100, choices=[(b'REQUESTED', b'Requested'), (b'INVITED', b'Invited'), (b'ACCEPTED', b'Accepted'), (b'REJECTED', b'Rejected'), (b'PAID', b'Paid')]),
            preserve_default=True,
        ),
    ]
