# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('kickaround', '0022_auto_20150407_1809'),
    ]

    operations = [
        migrations.AddField(
            model_name='kickaround',
            name='collect_payment',
            field=models.BooleanField(default=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='kickaroundgamemember',
            name='role',
            field=models.CharField(default=0, max_length=100, choices=[(b'2', b'active_organiser'), (b'1', b'passive_organiser'), (b'0', b'player')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='kickaroundmember',
            name='role',
            field=models.CharField(default=0, max_length=100, choices=[(b'2', b'active_organiser'), (b'1', b'passive_organiser'), (b'0', b'player')]),
            preserve_default=True,
        ),
    ]
