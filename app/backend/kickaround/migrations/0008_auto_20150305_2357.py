# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('kickaround', '0007_auto_20150304_0843'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='kickaround',
            name='next_game_on',
        ),
        migrations.AddField(
            model_name='kickaround',
            name='duration',
            field=models.FloatField(default=1.0, null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='kickaround',
            name='next_game_id',
            field=models.IntegerField(null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='kickaroundgame',
            name='game_status',
            field=models.CharField(default=(b'CREATED', b'Created/Pending Invites'), max_length=100, choices=[(b'CREATED', b'Created/Pending Invites'), (b'PENDING_RESPONSES', b'Pending Responses'), (b'GAME_CONFIRMED', b'Game Confirmed'), (b'GAME_CANCELLED', b'Game Cancelled'), (b'GAME_PLAYED', b'Game Played')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='kickaroundgamemember',
            name='status',
            field=models.CharField(default=(b'PENDING', b'Pending'), max_length=100, choices=[(b'REQUESTED', b'Requested'), (b'PENDING', b'Pending'), (b'INVITED', b'Invited'), (b'ACCEPTED', b'Accepted'), (b'REJECTED', b'Rejected'), (b'PAID', b'Paid')]),
            preserve_default=True,
        ),
    ]
