# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('kickaround', '0006_auto_20150304_0837'),
    ]

    operations = [
        migrations.AddField(
            model_name='kickaround',
            name='ka_privacy',
            field=models.CharField(default=(b'SEMI_OPEN', b'Semi open'), max_length=100, choices=[(b'OPEN', b'Open'), (b'SEMI_OPEN', b'Semi open'), (b'CLOSED', b'Closed,')]),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='kickaroundgame',
            name='game_privacy',
            field=models.CharField(default=(b'SEMI_OPEN', b'Semi open'), max_length=100, choices=[(b'OPEN', b'Open'), (b'SEMI_OPEN', b'Semi open'), (b'CLOSED', b'Closed,')]),
            preserve_default=True,
        ),
    ]
