# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('kickaround', '0021_auto_20150407_1546'),
    ]

    operations = [
        migrations.AlterField(
            model_name='kickaround',
            name='average_cost',
            field=models.FloatField(null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='kickaround',
            name='chase_payments_on',
            field=models.DateTimeField(null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='kickaround',
            name='day_of_week',
            field=models.CharField(blank=True, max_length=100, null=True, choices=[(b'4', b'friday'), (b'0', b'monday'), (b'5', b'saturday'), (b'6', b'sunday'), (b'3', b'thursday'), (b'1', b'tuesday'), (b'2', b'wednesday')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='kickaround',
            name='description',
            field=models.CharField(max_length=500, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='kickaround',
            name='duration',
            field=models.FloatField(default=1.0, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='kickaround',
            name='ka_privacy',
            field=models.CharField(default=1, max_length=100, null=True, blank=True, choices=[(b'2', b'closed'), (b'0', b'open'), (b'1', b'semi_open')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='kickaround',
            name='location',
            field=models.ForeignKey(blank=True, to='kickaround.Location', null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='kickaround',
            name='max_num_players',
            field=models.IntegerField(null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='kickaround',
            name='min_num_players',
            field=models.IntegerField(null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='kickaround',
            name='next_game_id',
            field=models.IntegerField(null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='kickaround',
            name='notify_hours_b4',
            field=models.IntegerField(default=4, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='kickaround',
            name='notify_on',
            field=models.DateTimeField(null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='kickaround',
            name='num_of_teams',
            field=models.IntegerField(default=2, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='kickaround',
            name='num_players_per_team',
            field=models.IntegerField(default=7, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='kickaround',
            name='pitch_hire_cost',
            field=models.FloatField(null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='kickaround',
            name='send_invites_on',
            field=models.DateTimeField(null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='kickaround',
            name='standard',
            field=models.CharField(default=0, max_length=100, null=True, blank=True, choices=[(b'2', b'competitive'), (b'0', b'everyone'), (b'1', b'semi_competitive')]),
            preserve_default=True,
        ),
    ]
