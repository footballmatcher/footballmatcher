# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('kickaround', '0013_auto_20150317_0750'),
    ]

    operations = [
        migrations.AddField(
            model_name='kickaroundpayment',
            name='date_created',
            field=models.DateTimeField(null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='kickaroundpayment',
            name='date_paid',
            field=models.DateTimeField(null=True),
            preserve_default=True,
        ),
    ]
