# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('kickaround', '0017_auto_20150324_0845'),
    ]

    operations = [
        migrations.AlterField(
            model_name='kickaround',
            name='day_of_week',
            field=models.CharField(max_length=100, null=True, choices=[(b'4', b'friday'), (b'0', b'monday'), (b'5', b'saturday'), (b'6', b'sunday'), (b'3', b'thursday'), (b'1', b'tuesday'), (b'2', b'wednesday')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='kickaround',
            name='standard',
            field=models.CharField(default=b'everyone_welcome', max_length=100, choices=[(b'competitive', b'competitive'), (b'everyone_welcome', b'everyone'), (b'semi_competitive', b'semi_competitive')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='kickaroundgame',
            name='game_privacy',
            field=models.CharField(default=1, max_length=100, choices=[(b'2', b'closed'), (b'0', b'open'), (b'1', b'semi_open')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='kickaroundgame',
            name='game_status',
            field=models.CharField(default=0, max_length=100, choices=[(b'0', b'created'), (b'3', b'game_cancelled'), (b'2', b'game_confirmed'), (b'4', b'game_played'), (b'1', b'pending_responses')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='kickaroundgamemember',
            name='role',
            field=models.CharField(default=1, max_length=100, choices=[(b'1', b'organiser'), (b'0', b'player')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='kickaroundgamemember',
            name='status',
            field=models.CharField(default=1, max_length=100, choices=[(b'3', b'accepted'), (b'2', b'invited'), (b'5', b'paid'), (b'1', b'pending'), (b'4', b'rejected'), (b'0', b'requested')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='kickaroundmember',
            name='member_status',
            field=models.CharField(default=0, max_length=100, choices=[(b'2', b'approved'), (b'4', b'deactivated'), (b'0', b'non_registered'), (b'1', b'pending'), (b'3', b'rejected')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='kickaroundmember',
            name='role',
            field=models.CharField(default=0, max_length=100, choices=[(b'1', b'organiser'), (b'0', b'player')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='kickaroundpayment',
            name='currency',
            field=models.CharField(default=0, max_length=20, choices=[(b'0', b'GBP'), (b'1', b'USD')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='kickaroundpayment',
            name='payment_status',
            field=models.CharField(default=0, max_length=100, choices=[(b'0', b'created'), (b'2', b'paid'), (b'1', b'requested')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='kickaroundpayment',
            name='payment_type',
            field=models.CharField(default=0, max_length=100, choices=[(b'0', b'cash'), (b'1', b'paypal')]),
            preserve_default=True,
        ),
    ]
