# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('kickaround', '0004_auto_20150214_1534'),
    ]

    operations = [
        migrations.AddField(
            model_name='kickaround',
            name='day_of_week',
            field=models.CharField(max_length=100, null=True, choices=[(b'MONDAY', b'Monday'), (b'TUESDAY', b'Tuesday'), (b'WEDNESDAY', b'Wednesday'), (b'THURSDAY', b'Thursday'), (b'FRIDAY', b'Friday'), (b'SATURDAY', b'Saturday'), (b'SUNDAY', b'Sunday')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='kickaround',
            name='avatar',
            field=models.ImageField(max_length=150, null=True, upload_to=b''),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='kickaroundgamemember',
            name='kickaround_game',
            field=models.ForeignKey(related_name='kickaround_game_members', to='kickaround.KickAroundGame'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='kickaroundside',
            name='side_avatar',
            field=models.ImageField(max_length=150, null=True, upload_to=b''),
            preserve_default=True,
        ),
    ]
