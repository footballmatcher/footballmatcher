# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='KickAround',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100)),
                ('avatar', models.ImageField(max_length=150, upload_to=b'')),
                ('description', models.CharField(max_length=500)),
                ('standard', models.CharField(default=(b'EVERYONE', b'Everyone Welcome'), max_length=100, choices=[(b'EVERYONE', b'Everyone Welcome'), (b'SEMI-COMPETITIVE', b'Semi Competitive'), (b'COMPETITIVE', b'Competitive')])),
                ('weekly_time', models.TimeField()),
                ('pitch_hire_cost', models.FloatField()),
                ('average_cost', models.FloatField()),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='KickAroundGame',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('start_time', models.DateTimeField()),
                ('duration', models.FloatField(default=1.0)),
                ('pitch_hire_cost', models.FloatField()),
                ('game_status', models.CharField(default=(b'CREATED', b'Created/Pending Invites'), max_length=100, choices=[(b'CREATED', b'Created/Pending Invites'), (b'PENDING_RESPONSES', b'Pending Responses'), (b'GAME_CONFIRMED', b'Game Confirmed'), (b'GAME_CANCELLED', b'Game Cancelled')])),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='KickAroundGameMember',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date_joined', models.DateField()),
                ('role', models.CharField(default=(b'PLAYER', b'Player'), max_length=100, choices=[(b'PLAYER', b'Player'), (b'ORGANISER', b'Organiser')])),
                ('status', models.CharField(default=(b'INVITED', b'Invited'), max_length=100, choices=[(b'INVITED', b'Invited'), (b'ACCEPTED', b'Accepted'), (b'REJECTED', b'Rejected'), (b'PAID', b'Paid')])),
                ('kickaround_game', models.ForeignKey(to='kickaround.KickAroundGame')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='KickAroundGamePayment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('payment_status', models.CharField(max_length=100, choices=[(b'REQUESTED', b'Requested'), (b'PAID', b'Paid'), (b'OUTSTANDING', b'Outstanding')])),
                ('payment_type', models.CharField(max_length=100, choices=[(b'CASH', b'Cash'), (b'PAYPAL', b'PayPal')])),
                ('amount', models.DecimalField(max_digits=19, decimal_places=2)),
                ('currency', models.CharField(max_length=20, choices=[(b'USD', b'US Dollar(s)'), (b'GBP', b'British Pound(s)')])),
                ('payee', models.ForeignKey(related_name='payee', to='kickaround.KickAroundGameMember')),
                ('payer', models.ForeignKey(related_name='payer', to='kickaround.KickAroundGameMember')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='KickAroundMember',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date_joined', models.DateField()),
                ('role', models.CharField(default=(b'PLAYER', b'Player'), max_length=100, choices=[(b'PLAYER', b'Player'), (b'ORGANISER', b'Organiser')])),
                ('member_status', models.CharField(default=(b'NON_REGISTERED', b'Non Registered'), max_length=100, choices=[(b'NON_REGISTERED', b'Non Registered'), (b'PENDING', b'Pending Approval'), (b'APPROVED', b'Approved'), (b'REJECTED', b'Rejected'), (b'DEACTIVATED', b'Deactivated')])),
                ('kickaround', models.ForeignKey(related_name='kickaround_members', to='kickaround.KickAround')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='KickAroundSide',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('side_name', models.CharField(max_length=100)),
                ('side_avatar', models.ImageField(max_length=150, upload_to=b'')),
                ('side_color', models.CharField(max_length=100)),
                ('kickaround', models.ForeignKey(related_name='kickaround_sides', to='kickaround.KickAround')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Location',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('sports_centre', models.CharField(max_length=500)),
                ('town_city', models.CharField(max_length=500)),
                ('postcode', models.CharField(max_length=500)),
                ('county', models.CharField(max_length=500)),
                ('country', models.CharField(max_length=500)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='kickaroundgamemember',
            name='kickaround_member',
            field=models.ForeignKey(to='kickaround.KickAroundMember'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='kickaroundgamemember',
            name='team',
            field=models.ForeignKey(default=None, blank=True, to='kickaround.KickAroundSide', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='kickaroundgame',
            name='game_members',
            field=models.ManyToManyField(to='kickaround.KickAroundMember', through='kickaround.KickAroundGameMember'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='kickaroundgame',
            name='kickaround',
            field=models.ForeignKey(to='kickaround.KickAround'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='kickaroundgame',
            name='location',
            field=models.ForeignKey(to='kickaround.Location'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='kickaround',
            name='location',
            field=models.ForeignKey(to='kickaround.Location'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='kickaround',
            name='members',
            field=models.ManyToManyField(to=settings.AUTH_USER_MODEL, through='kickaround.KickAroundMember'),
            preserve_default=True,
        ),
    ]
