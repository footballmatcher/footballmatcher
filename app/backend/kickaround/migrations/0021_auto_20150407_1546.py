# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('kickaround', '0020_kickaround_notify_on'),
    ]

    operations = [
        migrations.AlterField(
            model_name='kickaround',
            name='ka_privacy',
            field=models.CharField(default=1, max_length=100, null=True, choices=[(b'2', b'closed'), (b'0', b'open'), (b'1', b'semi_open')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='kickaround',
            name='standard',
            field=models.CharField(default=0, max_length=100, null=True, choices=[(b'2', b'competitive'), (b'0', b'everyone'), (b'1', b'semi_competitive')]),
            preserve_default=True,
        ),
    ]
