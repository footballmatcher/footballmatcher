from rest_framework import (permissions, views, response, parsers)

from kickaround.tasks.email_tasks import send_join_kickaround_emails, send_game_invitation_emails, \
                                            send_game_cancellation_emails, send_game_confirmation_emails
from kickaround.models import *

# Contains end points for tasks for managing a kickaround

# Creates a batch email task inviting people to join a kickaround, returns task id
class SendJoinKickaroundInvitationsEmails(views.APIView):
    parser_classes = (parsers.JSONParser,)
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request, kickaround_id=None, format=None):

        # Get kickaround from id
        ka = KickAround.objects.get(pk=kickaround_id)

        # Create batch email invite task
        invite_task = send_join_kickaround_emails.delay(request.data['emails'],ka, request.user)

        return response.Response({'email_addresses': request.data['emails'], 'task_id': invite_task.id, 'task_status': invite_task.status})

# Checks status of the Kick Around invite task (pending, success)
class GetJoinKickaroundInvitationTaskStatus(views.APIView):

    permission_classes = (permissions.IsAuthenticated,)

    def get(self,request, kickaround_id=None, task_id=None, format=None):

        # Get task from queue
        invite_task = send_join_kickaround_emails.AsyncResult(task_id)

        # Return status and result
        return response.Response({'task_id': invite_task.task_id, 'task_status': invite_task.status, 'task_result': invite_task.result})

# Creates a batch email task inviting people to this week's game
class SendGameInvitationEmails(views.APIView):
    parser_classes = (parsers.JSONParser,)
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request, kickaround_id=None, format=None):

        # Get kickaround from id
        ka = KickAround.objects.get(pk=kickaround_id)

        # Get upcoming game
        upcoming_game = KickAroundGame.objects.get(id=ka.next_game_id)

        # Create batch email invite task
        invite_task = send_game_invitation_emails.delay(ka, upcoming_game)

        return response.Response({'kickaround_id': kickaround_id, 'task_id': invite_task.id, 'task_status': invite_task.status})

# Checks status of the game invite task (pending, success)
class GetGameInvitationTaskStatus(views.APIView):

    permission_classes = (permissions.IsAuthenticated,)

    def get(self,request, kickaround_id=None, task_id=None, format=None):

        # Get task from queue
        invite_task = send_game_invitation_emails.AsyncResult(task_id)

        # Return status and result
        return response.Response({'task_id': invite_task.task_id, 'task_status': invite_task.status, 'task_result': invite_task.result})

# Creates a batch email to each game member confirming this week's game
class SendGameConfirmationEmails(views.APIView):
    parser_classes = (parsers.JSONParser,)
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request, kickaround_id=None, format=None):

        # Get kickaround from id
        ka = KickAround.objects.get(pk=kickaround_id)

        # Get upcoming game
        upcoming_game = KickAroundGame.objects.get(id=ka.next_game_id)

        # Create batch confirmation email task
        confirmation_task = send_game_confirmation_emails.delay(ka, upcoming_game)

        return response.Response({'kickaround_id': kickaround_id, 'task_id': confirmation_task.id, 'task_status': confirmation_task.status})

# Checks status of the game confirmation task (pending, success)
class GetGameConfirmationTaskStatus(views.APIView):

    permission_classes = (permissions.IsAuthenticated,)

    def get(self,request, kickaround_id=None, task_id=None, format=None):

        # Get task from queue
        confirmation_task = send_game_confirmation_emails.AsyncResult(task_id)

        # Return status and result
        return response.Response({'task_id': confirmation_task.task_id, 'task_status': confirmation_task.status, 'task_result': confirmation_task.result})

# Creates a batch email to each game member cancelling this week's game
class SendGameCancellationEmails(views.APIView):
    parser_classes = (parsers.JSONParser,)
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request, kickaround_id=None, format=None):

        # Get kickaround from id
        ka = KickAround.objects.get(pk=kickaround_id)

        # Get upcoming game
        upcoming_game = KickAroundGame.objects.get(id=ka.next_game_id)

        # Create batch confirmation email task
        cancellation_task = send_game_cancellation_emails.delay(ka, upcoming_game)

        return response.Response({'kickaround_id': kickaround_id, 'task_id': cancellation_task.id, 'task_status': cancellation_task.status})

# Checks status of the game cancellation invite task (pending, success)
class GetGameCancellationTaskStatus(views.APIView):

    permission_classes = (permissions.IsAuthenticated,)

    def get(self,request, kickaround_id=None, task_id=None, format=None):

        # Get task from queue
        cancellation_task = send_game_cancellation_emails.AsyncResult(task_id)

        # Return status and result
        return response.Response({'task_id': cancellation_task.task_id, 'task_status': cancellation_task.status, 'task_result': cancellation_task.result})
