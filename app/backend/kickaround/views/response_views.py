import string, random, datetime

from rest_framework import (views, response)

from itsdangerous import TimedJSONWebSignatureSerializer

from kickaround.models import *
from kickaround.config import TOKEN_SECRET

# Contains end points for email responses

def generate_fake_password():
    random_string = ''.join([random.choice(string.ascii_letters + string.digits) for n in xrange(64)])
    return random_string

# Respond to Invitation - creates kickaround membership and updates the status to either accepted or decline
class RespondToKickAroundJoinInvitation(views.APIView):

    # Anyone can call method
    permission_classes = []

    def get(self,request, format=None):

        # Token Decrypter
        s = TimedJSONWebSignatureSerializer(TOKEN_SECRET)

        # Decrypt Token
        data = s.loads(request.query_params['t']);

        # Extract data
        user_id = data['user_id'];
        kickaround_id = data['kickaround_id'];
        email_address = data['email_address'];
        action = data['action'];

        # Create new user if required
        if user_id == "new":
            # Create non registered user
            try:
                user = User(username=email_address, email=email_address, password=generate_fake_password())
                user.save()
            except:
                #user already exists
                user = User.objects.get(email=email_address)
                user_id = user.pk
        else:
            # Get existing user registered user
            user = User.objects.get(pk=user_id)

        # Get Kickaround
        ka = KickAround.objects.get(pk=kickaround_id)

        # If KickAround Member
        try:
            ka_mem = ka.members.get(user=user)
        # If Not KickAround Member
        except:
            ka_mem = KickAroundMember(user=user, kickaround=ka, date_joined=datetime.datetime.now())

        # User accepted invitation
        if(action == "accept"):
            # Set status to accepted
            ka_mem.member_status = "APPROVED"
        else:
            # Set status to declined
            ka_mem.member_status = "REJECTED"

        # Save kickaround membership
        ka_mem.save()

        # Return status and result
        return response.Response({'ka_mem.id': ka_mem.id, 'user_id': user_id, 'kickaround_id': kickaround_id, 'kickaround': ka.name, 'email_address': email_address, 'action': action, 'user_name': user.username,  'member_status': ka_mem.member_status})

# Respond to Game Invitation - updates kickaround game member status
class RespondToKickAroundGameInvitation(views.APIView):

    # Anyone can call method
    permission_classes = []

    def get(self,request, format=None):

        # Token Decrypter
        s = TimedJSONWebSignatureSerializer(TOKEN_SECRET)

        # Decrypt Token
        data = s.loads(request.query_params['t'])

        # Decrypt token
        game_member_id =  data['game_member_id'];
        action = data['action'];

        # Get Data
        game_member = KickAroundGameMember.objects.get(pk=game_member_id)

        # User accepted invitation
        if(action == "accept"):
            # Set status to accepted
            game_member.status = GameMemberStatusTypes.accepted.value()
        else:
            # Set status to rejected
            game_member.status = GameMemberStatusTypes.rejected.value()

        # Update database
        game_member.save()

# Respond to Game Confirmation
class RespondToGameConfirmation(views.APIView):

    # Anyone can call method
    permission_classes = []

    def get(self,request, format=None):

        print "Game confirmation response"

# Respond to Payment Due Email
class RespondToPaymentDue(views.APIView):

    # Anyone can call method
    permission_classes = []

    def get(self,request, format=None):

        print "Payment due response"

# Respond to Request to join email
class RespondToRequestToJoin(views.APIView):

    # Anyone can call method
    permission_classes = []

    def get(self,request, format=None):
        print "Request to join response"

