from django.contrib.auth.models import User
from rest_framework import (viewsets, permissions, generics)
from rest_framework import filters
from django_filters import FilterSet, CharFilter

from kickaround.serializers import *
from kickaround.permissions import IsKickAroundOrganiserOrReadOnly
from kickaround.models import *

# File contains end points for CRUD and list operations for the models

# Fields to filter Kickaround Lists with
class KickaroundFilter(FilterSet):
    sports_centre = CharFilter(name="location__sports_centre")
    town_city = CharFilter(name="location__town_city")
    class Meta:
        model = KickAround
        fields = ['name','day_of_week', 'ka_privacy', 'town_city', 'sports_centre']

# Fields to filter Game Lists with
class KickAroundGameFilter(FilterSet):
    kickaround_id = CharFilter(name="kickaround__pk")
    sports_centre = CharFilter(name="location__sports_centre")
    town_city = CharFilter(name="location__town_city")
    class Meta:
        model = KickAroundGame
        fields = ['kickaround_id', 'game_status', 'game_privacy', 'town_city', 'sports_centre']

# Fields to filter Kickaround Lists with
class PaymentFilter(FilterSet):
    kickaround_id = CharFilter(name="kickaround__pk")
    game_id = CharFilter(name="kickaround_game__pk")
    class Meta:
        model = KickAroundPayment
        fields = ['kickaround_id', 'game_id', 'payee','payer', 'payment_status', 'payment_type']

# Provides list services for KickAround
class KickAroundList(generics.ListCreateAPIView):
    model = KickAround
    # Check user is logged in
    permission_classes = (permissions.IsAuthenticated,)
    queryset = KickAround.objects.all()
    serializer_class = KickAroundSerializer
    filter_class = KickaroundFilter

    def get_queryset(self):
        """
        Optionally restricts the returned purchases to a given user,
        by filtering against a `user_id` query parameter in the URL.
        """
        queryset = KickAround.objects.all()
        user_id = self.request.QUERY_PARAMS.get('user_id', None)
        if user_id is not None:
            # Get user
            user = User.objects.get(id=user_id)

            # Get user's kickarounds
            queryset = user.kickaround_set.all()
        return queryset

# Provides detail services for KickAround
class KickAroundDetail(generics.RetrieveUpdateDestroyAPIView):
    # Check user is logged in
    permission_classes = (permissions.IsAuthenticated, IsKickAroundOrganiserOrReadOnly)
    queryset = KickAround.objects.all()
    serializer_class = KickAroundSerializer

# Provides fully deep KickAround with all children
class KickAroundKSDetail(generics.RetrieveAPIView):
    # Check user is logged in
    permission_classes = (permissions.IsAuthenticated,)
    queryset = KickAround.objects.all()
    serializer_class = KickAroundKitchenSinkSerializer
    filter_backends = (filters.DjangoFilterBackend,)

# Provides list of fully deep KickArounds with all children
class KickAroundKSList(generics.ListAPIView):
    # Check user is logged in
    permission_classes = (permissions.IsAuthenticated,)
    queryset = KickAround.objects.all()
    serializer_class = KickAroundKitchenSinkSerializer
    filter_class = KickaroundFilter

    def get_queryset(self):
        """
        Optionally restricts the returned purchases to a given user,
        by filtering against a `user_id` query parameter in the URL.
        """
        queryset = KickAround.objects.all()
        user_id = self.request.QUERY_PARAMS.get('user_id', None)
        if user_id is not None:
            # Get user
            user = User.objects.get(id=user_id)

            # Get user's kickarounds
            queryset = user.kickaround_set.all()
        return queryset

#Provides list and detail services for User
class UserViewSet(viewsets.ModelViewSet):
    permission_classes = [permissions.IsAuthenticated]
    queryset = User.objects.all()
    serializer_class = UserSerializer
    filter_backends = (filters.DjangoFilterBackend,)

#Provides list and detail services for Location
class LocationViewSet(viewsets.ModelViewSet):
    # Check user is logged in
    permission_classes = [permissions.IsAuthenticated]
    queryset = Location.objects.all()
    serializer_class = LocationSerializer

# Provides list and detail services for KickAroundMember
class KickAroundMemberViewSet(viewsets.ModelViewSet):
    # Check user is logged in
    permission_classes = [permissions.IsAuthenticated]
    queryset = KickAroundMember.objects.all()
    serializer_class = KickAroundMemberSerializer

# # Provides list and detail services for KickAroundGamer
class KickAroundGameViewSet(viewsets.ModelViewSet):
    # Check user is logged in
    permission_classes = [permissions.IsAuthenticated]
    queryset = KickAroundGame.objects.all()
    serializer_class = KickAroundGameSerializer
    filter_class = KickAroundGameFilter

    def get_queryset(self):
        """
        Optionally restricts the returned purchases to a given user,
        by filtering against a `user_id` query parameter in the URL.
        """
        queryset = KickAroundGame.objects.all()
        user_id = self.request.QUERY_PARAMS.get('user_id', None)

        if user_id is not None:

            # Get user
            user = User.objects.get(id=user_id)

            # Find kickarounds the user belongs to
            kas = user.kickaround_set.all()

            # Get user's games
            queryset = KickAroundGame.objects.filter(kickaround__in=kas)

        return queryset

# Provides list and detail services for KickAroundGameMember
class KickAroundGameMemberViewSet(viewsets.ModelViewSet):
    # Check user is logged ingu
    permission_classes = [permissions.IsAuthenticated]
    queryset = KickAroundGameMember.objects.all()
    serializer_class = KickAroundGameMemberSerializer
    filter_class = KickAroundGameFilter

# Provides list and detail services for KickAroundGameMember
class KickaroundPaymentViewSet(viewsets.ModelViewSet):
    # Check user is logged ingu
    permission_classes = [permissions.IsAuthenticated]
    queryset = KickAroundPayment.objects.all()
    serializer_class = KickAroundPaymentSerializer
    filter_class = PaymentFilter