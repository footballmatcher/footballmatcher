from __future__ import absolute_import

from celery import shared_task
from django.core import mail
from django.core.mail import EmailMessage, EmailMultiAlternatives
from django.template import Context
from django.template.loader import get_template
from itsdangerous import TimedJSONWebSignatureSerializer
from kickaround.models import *
from kickaround.config import TOKEN_SECRET

#generate join kickraround email
def generate_join_kickaround_email(connection, email_address, kickaround, organiser):
    # Get invite to join kickaround template
    template = get_template('join_ka_invite2.html')

    # Check that email address doesn't already exists
    if User.objects.filter(email=email_address).exists():
        user    = User.objects.get(email=email_address)
        user_id = user.id
    else:
        user_id = "new"

    # Set up the encrypter
    token_gen = TimedJSONWebSignatureSerializer(TOKEN_SECRET)

    # Create Accept token
    accept_token = token_gen.dumps({'user_id': user_id,  'kickaround_id':kickaround.id, 'email_address': email_address, 'action': 'accept'})
    accept_url = 'http://127.0.0.1:8000/email_response/join_kickaround?t=' + str(accept_token)

    # Create Decline token
    decline_token = token_gen.dumps({'user_id': user_id, 'kickaround_id':kickaround.id, 'email_address': email_address, 'action': 'decline'})
    decline_url = 'http://127.0.0.1:8000/email_response/join_kickaround?t=' + str(decline_token)

    # Create context for email
    context = Context({'org': organiser, 'kickaround': kickaround, 'accept_url': accept_url, 'decline_url': decline_url})

    # Render email
    html_content = template.render(context)

    # Construct an email message that uses the connection
    subject = organiser.first_name + " " +organiser.last_name+ ' - Join Kickaround Invitation'
    text_content = "You have been invited by "+ organiser.first_name +" "+organiser.last_name+" to the "+kickaround.name+" @ "+kickaround.location.sports_centre+" " + kickaround.location.town_city + "- Every "+kickaround.day_of_week+" " + str(kickaround.weekly_time)
    from_email = 'footballmatcher@gmail.com'
    to_emails = [str(email_address)]
    join_kickaround_email = EmailMultiAlternatives(subject, text_content, from_email, to_emails, connection=connection)
    join_kickaround_email.attach_alternative(html_content, "text/html")

    return join_kickaround_email

#generate invite to kickraround game email
def generate_invite_to_game_email(connection, kickaround, game, game_member):
    # Get invite to game template
    template = get_template('game_invite.html')

    # Set up the encrypter
    token_gen = TimedJSONWebSignatureSerializer(TOKEN_SECRET)

    # Create Accept token
    accept_token = token_gen.dumps({'game_member_id': game_member.id,  'kickaround_id':kickaround.id, 'game_id':game.id, 'action': 'accept'})
    accept_url = 'http://127.0.0.1:8000/email_response/game_invite?t=' + str(accept_token)

    # Create Decline token
    decline_token = token_gen.dumps({'game_member_id': game_member.id, 'kickaround_id':kickaround.id, 'game_id':game.id, 'action': 'decline'})
    decline_url = 'http://127.0.0.1:8000/email_response/game_invite?t=' + str(decline_token)

    # Create context for email
    context = Context({'kickaround': kickaround, 'game':game,  'accept_url': accept_url, 'decline_url': decline_url})

    # Render email
    html_content = template.render(context)

    # Construct an email message that uses the connection
    subject = "You have been invited you to this week's " + kickaround.name + " game"
    text_content = "You have been invited this week's "+kickaround.name+" game @ "+kickaround.location.sports_centre+" " + kickaround.location.town_city + "- this "+str(game.start_time)
    from_email = 'footballmatcher@gmail.com'
    to_emails = [str(game_member.kickaround_member.user.email)]
    invite_to_game_email = EmailMultiAlternatives(subject, text_content, from_email, to_emails, connection=connection)
    invite_to_game_email.attach_alternative(html_content, "text/html")

    print "Game invite Email created"

    return invite_to_game_email


#generate request to join kickaround email
def generate_request_to_join_email(connection, kickaround, organiser, user):
    # Get template
    template = get_template('request_to_join.html')

    # Set up the encrypter
    token_gen = TimedJSONWebSignatureSerializer(TOKEN_SECRET)

    # Create Accept token
    accept_token = token_gen.dumps({'user': user.id, 'org': organiser.id, 'kickaround_id':kickaround.id, 'action': 'accept'})
    accept_url = 'http://127.0.0.1:8000/email_response/request_to_join?t=' + str(accept_token)

    # Create Decline token
    decline_token = token_gen.dumps({'user_id': user.id, 'org': organiser.id, 'kickaround_id':kickaround.id, 'action': 'decline'})
    decline_url = 'http://127.0.0.1:8000/email_response/request_to_join?t=' + str(decline_token)

    # Create context for email
    context = Context({'user': user, 'organiser': organiser, 'kickaround': kickaround, 'accept_url': accept_url, 'decline_url': decline_url})

    # Render email
    html_content = template.render(context)

    # Construct an email message that uses the connection
    subject = user.first_name + " " +user.last_name+ ' - Request to Join Kickaround'
    text_content = "" + user.first_name + " " + user.last_name+  " has requested to join the "+kickaround.name+" @ "+kickaround.location.sports_centre+" " + kickaround.location.town_city + "- Every "+kickaround.day_of_week+" " + str(kickaround.weekly_time)
    from_email = 'footballmatcher@gmail.com'
    to_emails = [str(user.email)]
    request_to_join_email = EmailMultiAlternatives(subject, text_content, from_email, to_emails, connection=connection)
    request_to_join_email.attach_alternative(html_content, "text/html")

    return request_to_join_email

#generate game confirmation email
def generate_game_confirmation_email(connection, kickaround, game, game_member):
    # Get template
    template = get_template('game_confirmation.html')

    # Set up the encrypter
    token_gen = TimedJSONWebSignatureSerializer(TOKEN_SECRET)

    # Create Accept token
    pay_now_token = token_gen.dumps({'game_member_id': game_member.id, 'game_id':game.id, 'action': 'paynow'})
    pay_now_url = 'http://127.0.0.1:8000/email_response/payment_response?t=' + str(pay_now_token)

    # Create Decline token
    pay_later_token = token_gen.dumps({'game_member_id': game_member.id, 'game_id':game.id, 'action': 'paylater'})
    pay_later_url = 'http://127.0.0.1:8000/email_response/payment_response?t=' + str(pay_later_token)

    # Create context for email
    context = Context({'member_id': game_member.id, 'kickaround': kickaround, 'game': game,  'pay_now_url': pay_now_url, 'pay_later_url': pay_later_url})

    # Render email
    html_content = template.render(context)

    # Construct an email message that uses the connection
    subject = "Game Confirmation - " + kickaround.name + " - " + str(game.start_time)
    text_content = "Game Confirmation - " + kickaround.name + " - " + str(game.start_time)
    from_email = 'footballmatcher@gmail.com'
    to_emails = [str(game_member.kickaround_member.user.email)]
    game_confirmation_email = EmailMultiAlternatives(subject, text_content, from_email, to_emails, connection=connection)
    game_confirmation_email.attach_alternative(html_content, "text/html")

    return game_confirmation_email

#generate game confirmation email with sides
def generate_game_confirmation_with_sides_email(connection, kickaround, game, game_member):
    # Get template
    template = get_template('game_confirmation.html')

    # Set up the encrypter
    token_gen = TimedJSONWebSignatureSerializer(TOKEN_SECRET)

    # Create Accept token
    pay_now_token = token_gen.dumps({'game_member_id': game_member.id, 'game_id':game.id, 'action': 'paynow'})
    pay_now_url = 'http://127.0.0.1:8000/email_response/payment_response?t=' + str(pay_now_token)

    # Create Decline token
    pay_later_token = token_gen.dumps({'game_member_id': game_member.id,  'game_id':game.id, 'action': 'paylater'})
    pay_later_url = 'http://127.0.0.1:8000/email_response/payment_response?t=' + str(pay_later_token)

    # Create context for email
    context = Context({'game_member_id': game_member, 'kickaround': kickaround, 'game': game,  'pay_now_url': pay_now_url, 'pay_later_url': pay_later_url})

    # Render email
    html_content = template.render(context)

    # Construct an email message that uses the connection
    subject = "Game Confirmation - " + kickaround.name + " - " + str(game.start_time)
    text_content = "Game Confirmation - " + kickaround.name + " - " + str(game.start_time)
    from_email = 'footballmatcher@gmail.com'
    to_emails = [str(game_member.kickaround_member.user.email)]
    game_confirmation_with_sides_email = EmailMultiAlternatives(subject, text_content, from_email, to_emails, connection=connection)
    game_confirmation_with_sides_email.attach_alternative(html_content, "text/html")

    return game_confirmation_with_sides_email

#generate game cancellation email
def generate_game_cancellation_email(connection, kickaround, game, organiser, user):
    # Get template
    template = get_template('game_cancellation.html')

    # Create context for email
    context = Context({'user': user, 'organiser': organiser, 'kickaround': kickaround, 'game': game})

    # Render email
    html_content = template.render(context)

    # Construct an email message that uses the connection
    subject = "Game Cancellation - " + kickaround.name + " - " + game.start_time
    text_content = "Game Cancellation - " + kickaround.name + " - " + game.start_time
    from_email = 'footballmatcher@gmail.com'
    to_emails = [str(user.email)]
    game_cancellation_email = EmailMultiAlternatives(subject, text_content, from_email, to_emails, connection=connection)
    game_cancellation_email.attach_alternative(html_content, "text/html")

    return game_cancellation_email

#generate payments due email
def generate_payments_due_email(connection, kickaround, payee, payer, amount):
    # Get template
    template = get_template('payment_due.html')

    # Set up the encrypter
    token_gen = TimedJSONWebSignatureSerializer(TOKEN_SECRET)

    # Create Pay online token
    pay_online_token = token_gen.dumps({'payee_id': payee.id, 'payer_id': payer.id, 'action': 'paynow'})
    pay_online_url = 'http://127.0.0.1:8000/email_response/payment_status_response?t=' + str(pay_online_token)

    # Create Paid by cash token
    paid_cash_token = token_gen.dumps({'payee_id': payee.id, 'payer_id': payer.id, 'action': 'paidcash'})
    paid_cash_url = 'http://127.0.0.1:8000/email_response/payment_status_response?t=' + str(paid_cash_token)

    # Create context for email
    context = Context({'amount': amount, 'payee': payee, 'payer': payer, 'kickaround': kickaround, 'paid_url': paid_cash_url, 'pay_later_url': pay_online_url})

    # Render email
    html_content = template.render(context)

    # Construct an email message that uses the connection
    subject = "Payment Due - " + kickaround.name
    text_content = "Payment Due - " + kickaround.name
    from_email = 'footballmatcher@gmail.com'
    to_email = [str(payer.email)]
    print "To Email: " + str(payee.email)
    print "From Email: " + from_email
    payments_due_email = EmailMultiAlternatives(subject, text_content, from_email, to_email, connection=connection)
    payments_due_email.attach_alternative(html_content, "text/html")

    print "Payment email generated"

    return payments_due_email

@shared_task
# Task called when KA is initially created, invites to join are sent to all the emails provided
def send_join_kickaround_emails(email_addresses, kickaround, organiser):
        # Get Email connection
    connection = mail.get_connection()

    # Manually open the connection
    connection.open()

    # Get invite to join kickaround template
    template = get_template('join_ka_invite.html')

    # List of emails to send
    emails = []

    # Loop through invite email addresses
    for email_address in email_addresses:
        emails.append(generate_join_kickaround_email(connection, email_address, kickaround, organiser))

    # Send the two emails in a single call -
    connection.send_messages(emails)

    # The connection was already open so send_messages() doesn't close it.
    # We need to manually close the connection.
    connection.close()

@shared_task
# Send game confirmation emails for game
def send_game_invitation_emails(kickaround, game):
    # Get Email connection
    connection = mail.get_connection()

    # Manually open the connection
    connection.open()

    # Get Kickaround Game Members
    members = KickAroundGameMember.objects.filter(kickaround_game=game)

    # List of emails to send
    emails = []

    # Loop through invite email addresses
    for member in members:
        emails.append(generate_invite_to_game_email(connection, kickaround, game, member))

        # Update game member status
        member.status = GameMemberStatusTypes.invited.value
        member.save()

    # Send the two emails in a single call -
    connection.send_messages(emails)

    print "Game invitation emails sent"

    # The connection was already open so send_messages() doesn't close it.
    # We need to manually close the connection.
    connection.close()

@shared_task
# Send game confirmation emails for game
def send_game_confirmation_emails(kickaround, game):
    # Get Email connection
    connection = mail.get_connection()

    # Manually open the connection
    connection.open()

    # Get Kickaround Game Members
    members = KickAroundGameMember.objects.filter(kickaround_game=game)

    # List of emails to send
    emails = []

    # Loop through invite email addresses
    for member in members:
        emails.append(generate_game_confirmation_email(connection, kickaround, game, member))

    # Send the two emails in a single call -
    connection.send_messages(emails)

    print "Game confirmation emails sent"

    # The connection was already open so send_messages() doesn't close it.
    # We need to manually close the connection.
    connection.close()

@shared_task
# Send game canellation emails for game
def send_game_cancellation_emails(kickaround, game):
    # Get Email connection
    connection = mail.get_connection()

    # Manually open the connection
    connection.open()

    # Get Kickaround Game Members
    members = KickAroundGameMember.objects.filter(kickaround_game=game)

    # List of emails to send
    emails = []

    # Loop through invite email addresses
    for member in members:
        emails.append(generate_game_cancellation_email(connection, kickaround, game, member))

    # Send the two emails in a single call -
    connection.send_messages(emails)

    # Output
    print "Game cancellation emails sent"

    # The connection was already open so send_messages() doesn't close it.
    # We need to manually close the connection.
    connection.close()

@shared_task
# Send request to join email for game
def send_request_to_join_email(kickaround, user):
    # Get Email connection
    connection = mail.get_connection()

    # Manually open the connection
    connection.open()

    # Get Organisers of Kickaround
    organisers = KickAroundMember.objects.filter(kickaround=kickaround).filter(role=RolesTypes.organiser.value)

    # Emails to send
    emails = []

    # Loop through invite email addresses
    for organiser in organisers:
        emails.append(generate_request_to_join_email(connection, kickaround,organiser, user))

    # Send the two emails in a single call -
    connection.send_messages(emails)

    # Output
    print "Request to join email sent"

    # Manually close the connection.
    connection.close()



@shared_task
# Send payment due emails for kickaround for all organisers
def send_games_payment_due_emails(kickaround, game):
    # Get Email connection
    connection = mail.get_connection()


    # Manually open the connection
    connection.open()

    # Get Outstanding Payments for Game
    payments = game.game_payments.filter(payment_status=PaymentStatusTypes.created.value)

    # List of emails to send
    emails = []

    # Loop through invite email addresses
    for payment in payments:
        # Create email from template
        emails.append(generate_payments_due_email(connection, kickaround, payment.payee, payment.payer, payment.amount))

    # Send the two emails in a single call
    connection.send_messages(emails)

    print "Game payment request sent"

    # The connection was already open so send_messages() doesn't close it.
    # We need to manually close the connection.
    connection.close()

# Calculate total payments
def calculate_total_payments(payments):
    total_amount = 0
    for payment in payments:
        total = total + payment.amount
    return total_amount

@shared_task
# Send payment due emails for kickaround for all organisers
def send_organisers_outstanding_payments_due_emails(kickaround, organiser):
    # Get Email connection
    connection = mail.get_connection()

    # Manually open the connection
    connection.open()

    # List of emails to send
    emails = []

    # Loop through invite email addresses
    for member in kickaround.members.all():
        # Get all outstanding payments for member
        mem_payments = kickaround.payments.filter(payee=organiser).filter(payer=member).filter(payment_status=PaymentStatusTypes.created.value)

        # Calculate total amount owed
        total_balance = calculate_total_payments(mem_payments)

        # Create email from template if there is an outstanding balance
        if(len(mem_payments) > 0):
            emails.append(generate_payments_due_email(connection, kickaround, organiser, member, total_balance))

    print "All emails generated"

    # Send the two emails in a single call
    connection.send_messages(emails)

    print "Outstanding payment requests sent"

    # The connection was already open so send_messages() doesn't close it.
    # We need to manually close the connection.
    connection.close()

@shared_task
# Send payment due emails for kickaround for all organisers
def send_kickaround_outstanding_payments_due_emails(kickaround):
    # Get Email connection
    connection = mail.get_connection()

    # Manually open the connection
    connection.open()

    # List of email addresses
    emails = []

    for organiser in KickAroundMember.filter(kickaround=kickaround).filter(role=RolesTypes.organiser.value):

        # Loop through invite email addresses
        for member in kickaround.members:
            # Get all outstanding payments for member
            mem_payments = kickaround.payments.filter(payee=organiser).filter(payer=member).filter(payment_status=PaymentStatusTypes.created.value)

            # Calculate total amount owed
            total_balance = calculate_total_payments(mem_payments)

            # Create email from template
            emails.append(generate_payments_due_email(connection, kickaround, organiser, member, total_balance))

    # Send the two emails in a single call
    connection.send_messages(emails)

    # The connection was already open so send_messages() doesn't close it.
    # We need to manually close the connection.
    connection.close()

