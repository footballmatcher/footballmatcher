from __future__ import absolute_import
import datetime, time

from celery import shared_task
from kickaround.models import *
from kickaround.enums import *
from kickaround.tasks.email_tasks import *

TOKEN_SECRET="gokul"

# Returns a dict of the current day of the week plus an time range between now and an hours time
def get_date_time_range():
    # Get current date time
    now = datetime.datetime.now()

    # FIXME - SET TO SPECIFIC KA TIME - MONDAY NIGHT 8PM
    # Zero the minutes, seconds and hour
    now = now.replace(month=3, day=2, hour=20,minute=0, second=0, microsecond=0)

    # Get datetime in an hour
    now_plus_1hr = now + datetime.timedelta(hours=+1)

    # Get date time of next week
    next_week = now + datetime.timedelta(days=+7)

    # Create dictionary of day of week and the time range
    time_range_dict = {'day_of_week': now.weekday(), 'now': now, 'now_plus_1hr': now_plus_1hr, 'next_week':next_week}

    return time_range_dict

# Create all the weekly games for every KA that are scheduled exactly a week from this hour
@shared_task
def scheduled_create_next_week_games_for_all_kas():

    # Get today, the search time range
    time_range_dict = get_date_time_range()
    day_of_week = time_range_dict['day_of_week']
    now = time_range_dict['now']
    now_plus_1hr = time_range_dict['now_plus_1hr']
    next_week_time = time_range_dict['next_week']

    print "Get Kickarounds that have games scheduled to start between " + str(now.time()) + " and " + str(now_plus_1hr.time()) + " this " + str(day_of_week[1])

    # Get KickArounds that are schedule a week from today within the time range
    kas = KickAround.objects.filter(day_of_week=day_of_week).filter(weekly_time__gte=now.time(), weekly_time__lt=now_plus_1hr.time())

    # Loop through kickArounds, create next kickaround game
    for ka in kas:
        print "Closing current game for " + ka.name

        # Get current games that are currently being played
        current_games = ka.kickaround_games.filter(start_time__lte = now).filter(game_status=GameStatusTypes.game_confirmed.value)

        # Loop through current games (should only be one) and set the status to one
        for current_game in current_games:
            # Set game status to played
            current_game.game_status = game_status=GameStatusTypes.game_played.value()
            # Update database
            current_game.save()

        # Create next weeks kickaround game
        next_week_game = KickAroundGame(kickaround = ka, game_status = GameStatusTypes.created.value, location = ka.location, duration = ka.duration, pitch_hire_cost = ka.pitch_hire_cost)
        # set date to next week date
        next_week_game.start_time = next_week_time
        # set time to kickaround's weekly time
        next_week_game.start_time.replace(hour=ka.weekly_time.hour, minute=ka.weekly_time.minute, second=0, microsecond=0)

        # Check that current game doesn't already exist
        if(not ka.kickaround_games.filter(start_time=next_week_game.start_time).exists()):
            print "Created next week's game for " + ka.name + " on " + str(next_week_game.start_time)

            # Save new kickaround game to database
            next_week_game.save()

            # Get Kickaround members
            ka_members = KickAroundMember.objects.filter(kickaround=ka)

            # Add members to kickaround
            for member in ka_members:
                # Create new game member
                game_member = KickAroundGameMember(kickaround=ka, user=member.user, role=member.role, status=GameMemberStatusTypes.invited.value)
                # Save new game member
                game_member.save()
        else:
            print "Next week's game already created for " + ka.name + " on " + str(next_week_game.start_time)

        # Update Kickaround fields
        ka.next_game_id = next_week_game.pk
        ka.send_invites_on = next_week_game.start_time + datetime.timedelta(days=-ka.send_invites_days_b4)
        ka.notify_on = next_week_game.start_time + datetime.timedelta(hours=-ka.notify_hours_b4)
        ka.save()

# Loop through kickarounds with send invitation scheduled within the next hour and invite players to join kickaround game based on kickaround settings
@shared_task
def scheduled_invite_players_to_game_for_all_kas():
    # Get current date time
    now = datetime.datetime.now()
    # Zero the minutes, seconds and hour
    now = now.replace(minute=0, second=0, microsecond=0)
    # Get datetime in an hour
    now_plus_1hr = now + datetime.timedelta(hours=+1)

    # Get kickarounds that have invitations due to be sent in the next hour and have the automate option set
    kas = KickAround.objects.filter(automate_send_invites=True).filter(send_invites_on__gre=now, send_invites_on__lt=now_plus_1hr)

    # Loop through kickarounds, send invites for the members of the upcoming kickaround game
    for ka in kas:
        # Get upcoming game
        upcoming_game = ka.kickaround_games.get(pk=ka.next_game_id)

        # Send invites to all members
        send_game_invitation_emails(ka, upcoming_game)

        # Update game status to pending responses
        upcoming_game.game_status = GameStatusTypes.pending_responses.value
        upcoming_game.save()

# Invite players to join game based on kickaround settings
@shared_task
def manual_invite_players_to_game_for_ka(kickaround_id):
    # Get kickaround
    ka = KickAround.objects.get(pk=kickaround_id)

    # Get upcoming game
    upcoming_game = ka.kickaround_games.get(pk=ka.next_game_id)

    # Send invites to all members
    send_game_invitation_emails(ka, upcoming_game)

    # Update game status to invited
    upcoming_game.game_status = GameStatusTypes.pending_responses.value
    upcoming_game.save()

# Generate payment info for a game
def generate_payments_for_confirmed_game_members(ka, game, game_members):

    # Get Active organiser (should only be one)
    try:
        active_organiser = game.kickaround_game_members.filter(role=RolesTypes.active_organiser.value)[0]
    except:
        print "Error - no active organiser for game"
        active_organiser = None

    for game_member in game_members:

        # If there is an active organiser
        if active_organiser is not None:

            payment = KickAroundPayment(
                              kickaround = ka,
                              kickaround_game = game,
                              payee = active_organiser.member.user,
                              payer = game_member.member.user,
                              amount = game.cost_per_player,
                              currency = CurrencyTypes.GBP.value,
                              date_created = datetime.datetime.now(), payment_status = PaymentStatusTypes.created.value)
            # Save
            payment.save()

# Send game confirmation email (with side information)
@shared_task
def scheduled_confirm_cancel_games():
    # Get current date time
    now = datetime.datetime.now()
    # Zero the minutes, seconds and hour
    now = now.replace(minute=0, second=0, microsecond=0)
    # Get datetime in an hour
    now_plus_1hr = now + datetime.timedelta(hours=+1)

    # Get kickarounds that are due to be confirmed in the next hour
    kas = KickAround.objects.filter(notify_on__gre=now, notify__lt=now_plus_1hr)

    # loop through kickarounds
    for ka in kas:
        # Get current game in kickaround
        upcoming_game = KickAroundGame.objects.get(pk=ka.next_game_id)

        # If game isn't confirmed
        if upcoming_game.game_status == GameStatusTypes.created.value:

            # Get Confirmed Game Member
            game_members = upcoming_game.kickaround_game_members.filter(status=GameMemberStatusTypes.accepted.value)

            # If number of confirmed is greater than minimum required, confirmed
            if(len(game_members) >= ka.min_num_players):

                # If automate_game_confirmation is enabled
                if ka.automate_game_confirmation:

                    # Send Game confirmation email
                    send_game_confirmation_emails(ka, upcoming_game)

                    # Update game status
                    upcoming_game.game_status = GameStatusTypes.game_confirmed.value

                    # Generate payments if collect_payment is enabled
                    if ka.collect_payment:
                        generate_payments_for_confirmed_game_members(ka, upcoming_game, game_members)

                    # Save to database
                    upcoming_game.save()

            # Too few players
            else :

                # If automate_game_cancellation is enabled
                if ka.automate_game_cancellation:

                    # Send Game confirmation email
                    send_game_cancellation_emails(ka, upcoming_game)

                    # Update game status
                    upcoming_game.game_status = GameStatusTypes.game_cancelled.value

                    # Cancellation reason
                    upcoming_game.cancellation_reason = "Too few players confirmed"

                    # Save to database
                    upcoming_game.save()

                # TODO MANUALLY NOTIFY ORGANISER
                # else:

# Manually send game confirmation email
@shared_task
def manually_confirm_game(kickaround_id):
    ka = KickAround.objects.get(pk=kickaround_id)

    # Get current game in kickaround
    upcoming_game = KickAroundGame.objects.get(pk=ka.next_game_id)

    # Send Game confirmation email
    send_game_confirmation_emails(ka, upcoming_game)

    # Update game status
    upcoming_game.game_status = GameStatusTypes.game_confirmed.value

    # Get Confirmed Game Member
    game_members = upcoming_game.kickaround_game_members.filter(status=GameMemberStatusTypes.accepted.value)

    # Generate payments if collect_payment is enabled
    if ka.collect_payment:
        generate_payments_for_confirmed_game_members(ka, upcoming_game, game_members)

    # Save to database
    upcoming_game.save()

# Manually send game cancellation email
@shared_task
def manually_cancel_game(kickaround_id, cancellation_reason):
    ka = KickAround.objects.get(pk=kickaround_id)

    # Get current game in kickaround
    upcoming_game = KickAroundGame.objects.get(pk=ka.next_game_id)

    # Send Game confirmation email
    send_game_cancellation_emails(ka, upcoming_game)

    # Update game status
    upcoming_game.game_status = GameStatusTypes.game_cancelled.value

    # Cancellation reason
    upcoming_game.cancellation_reason = cancellation_reason

    # Save to database
    upcoming_game.save()


def add(a, b):
    return a + b