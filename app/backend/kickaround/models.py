from django.db import models
from django.contrib.auth.models import User

from kickaround.enums import *

# Make User Email addresses unique in the database
User._meta.local_fields[4].__dict__['_unique'] = True

# Location: This stores the address of where the kickaround is played
class Location(models.Model):
    sports_centre = models.CharField(max_length=500)
    town_city =  models.CharField(max_length=500)
    postcode = models.CharField(max_length=500)
    county  = models.CharField(max_length=500)
    country = models.CharField(max_length=500)
    def __unicode__(self):
        return self.sports_centre

# KickAround: A group of people who play casual football each week
class KickAround(models.Model):
    # KickAround Name
    name = models.CharField(max_length=100)
    # KickAround Picture
    avatar = models.ImageField(max_length=150, null=True)
    # Description of the group of people
    description = models.CharField(max_length=500, null=True, blank=True)
    # Standard of the football played
    standard = models.CharField(choices=StandardTypes.choices(), max_length=100, default=StandardTypes.everyone.value, null=True, blank=True)
    # Players and organisers of kickaround
    members = models.ManyToManyField(User, through='KickAroundMember')
    # Location of sports centre
    location = models.ForeignKey(Location, null=True, blank=True)
    # Time of the week game is played
    weekly_time = models.TimeField()
    # Duration of game
    duration = models.FloatField(default=1.0, null=True, blank=True)
    # Day of week the game is played
    day_of_week = models.CharField(choices=DayOfWeekTypes.choices(), max_length=100, null=True, blank=True)
    # Pitch hire cost
    pitch_hire_cost = models.FloatField(null=True, blank=True)
    # Average cost per player
    average_cost = models.FloatField(null=True, blank=True)
    # Numbers per team
    num_players_per_team = models.IntegerField(default=7, null=True, blank=True)
    # Number of teams
    num_of_teams = models.IntegerField(default=2, null=True, blank=True)
    # Maximum number of players before putting on waiting list
    max_num_players = models.IntegerField(null=True, blank=True)
    # Enforce maximum number, put people on waiting list
    enforce_max_num_players = models.BooleanField(default=False)
    # Minimum number of players for game to go ahead
    min_num_players = models.IntegerField(null=True, blank=True)
    # Notify organiser the game the game has too few players
    notify_org_too_few_players = models.BooleanField(default=True)
    # How many hours before game, before notifying the organiser the game has too few players
    notify_hours_b4 = models.IntegerField(null=True, default=4, blank=True)
    # Date and time to notify everyone
    notify_on = models.DateTimeField(null=True, blank=True)
    # Automate game confirmation if the minimum number of players have been reached
    automate_game_confirmation = models.BooleanField(default=True)
    # Automate game cancellation if there is not the required minimum number of players
    automate_game_cancellation = models.BooleanField(default=True)
    # Next weeks kickaround game id
    next_game_id = models.IntegerField(null=True, blank=True)
    # Date and time to send invitations
    send_invites_on = models.DateTimeField(null=True, blank=True)
    # Number of days before game, to send invitations
    send_invites_days_b4 = models.IntegerField(default=2)
    # Send invitations automatically
    automate_send_invites = models.BooleanField(default=True)
    # Collect payment
    collect_payment = models.BooleanField(default=True)
    # Send payment emails chasing payment for those who haven't paid
    automate_chase_payment = models.BooleanField(default=True)
    # Number of days after game before sending chase payment emails
    chase_payments_after = models.IntegerField(default=2)
    # Send chase payment emails on
    chase_payments_on= models.DateTimeField(null=True, blank=True)
    # Kickaround Privacy Settings, default equals semi open
    ka_privacy = models.CharField(choices=KAPrivacyTypes.choices(), max_length=100, default=KAPrivacyTypes.semi_open.value, null=True, blank=True)

    def __unicode__(self):
        return self.name

# KickaroundMember: Is a player or organiser who is member of the KickAround
class KickAroundMember(models.Model):
    user = models.ForeignKey(User)
    kickaround = models.ForeignKey(KickAround, related_name='kickaround_members')
    date_joined = models.DateField()
    role = models.CharField(choices=RolesTypes.choices(), max_length=100, default=RolesTypes.player.value)
    member_status = models.CharField(choices=MemberStatusTypes.choices(), max_length=100, default=MemberStatusTypes.non_registered.value)

    def __unicode__(self):
        return self.user.username

# KickaroundGame: Each week a kickaround game is created. The players invited are then associated with the game for that week.
class KickAroundGame(models.Model):
    kickaround = models.ForeignKey(KickAround, related_name='kickaround_games')
    location = models.ForeignKey(Location, null=True)
    start_time = models.DateTimeField()
    duration = models.FloatField(default=1.0, null=True)
    pitch_hire_cost = models.FloatField(null=True)
    cost_per_player = models.FloatField(null=True)
    game_status = models.CharField(choices=GameStatusTypes.choices(), max_length=100, default=GameStatusTypes.created.value)
    game_members = models.ManyToManyField(KickAroundMember, through='KickAroundGameMember')
    # Game privacy, defaults to semi open - only organiser and players can invite
    game_privacy = models.CharField(choices=GamePrivacyTypes.choices(), max_length=100, default=GamePrivacyTypes.semi_open.value)
    # Reason for cancelling game
    cancellation_reason = models.CharField(max_length=500, null=True)
    def __unicode__(self):
        return str(self.start_time)

# KickaroundSide: Equivalent to organiser choosing sides for each game. Multiple sides can be picked per game
class KickAroundSide(models.Model):
    side_name = models.CharField(max_length=100,null=True)
    side_avatar = models.ImageField(max_length=150,null=True)
    side_color = models.CharField(max_length=100)
    kickaround = models.ForeignKey(KickAround, related_name='kickaround_sides')

    def __unicode__(self):
        return self.side_name

# KickaroundGameMember: Is player or organiser that has been invited to play in this weeks game
class KickAroundGameMember(models.Model):
    kickaround_member = models.ForeignKey(KickAroundMember)
    kickaround_game = models.ForeignKey(KickAroundGame, related_name='kickaround_game_members')
    date_joined = models.DateField()
    team = models.ForeignKey(KickAroundSide, null=True, blank=True, default=None)
    role = models.CharField(choices=RolesTypes.choices(), max_length=100, default=RolesTypes.player.value)
    status = models.CharField(choices=GameMemberStatusTypes.choices(), max_length=100, default=GameMemberStatusTypes.pending.value)

    def __unicode__(self):
        return self.kickaround_member.user.username

# KickaroundPayment: Every time a player accepts an invitation to a kickaround game, a payment is created.
class KickAroundPayment(models.Model):
    kickaround = models.ForeignKey(KickAround, related_name='payments', null=True)
    kickaround_game = models.ForeignKey(KickAroundGame, related_name='game_payments', null=True)
    payee = models.ForeignKey(User, related_name='payee')
    payer = models.ForeignKey(User, related_name='payer')
    amount = models.DecimalField(decimal_places=2, max_digits=19)
    currency = models.CharField(choices=CurrencyTypes.choices(), max_length=20,  default=CurrencyTypes.GBP.value)
    date_created = models.DateTimeField(null=True)
    date_paid = models.DateTimeField(null=True)
    payment_status = models.CharField(choices=PaymentStatusTypes.choices(), max_length=100,  default=PaymentStatusTypes.created.value)
    payment_type = models.CharField(choices=PaymentTypes.choices(), max_length=100, default=PaymentTypes.cash.value)

    def __unicode__(self):
        #return  ' <- (%s %s) ' % (str(self.amount), self.currency)
        return (unicode(self.payee) +
                ' <- (%s %s) ' % (str(self.amount), self.currency) +
                unicode(self.payer))
