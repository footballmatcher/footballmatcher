from rest_framework import permissions
from .models import KickAroundMember

class IsKickAroundOrganiserOrReadOnly(permissions.BasePermission):
    """
    Give write permission to organiser,  if organiser of the KickAround
    """
    def has_object_permission(self, request, view, obj):
        # Is kickaround member?
        #import pdb;pdb.set_trace()
        if KickAroundMember.objects.filter(user=request.user, kickaround=obj).exists():
            member = KickAroundMember.objects.filter(user=request.user, kickaround=obj)
            # Any player belonging to KickAround has read permissions to any request
            # so we'll always allow GET, HEAD or OPTIONS requests.
            if request.method in permissions.SAFE_METHODS:
                return True
            elif member.role == 'Organiser':
                return True
            else:
                return False
        else:
            return False
