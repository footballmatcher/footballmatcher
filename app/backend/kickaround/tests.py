from django.test import TestCase
from kickaround.tasks.scheduled_tasks import add

class KickaroundTests(TestCase):
    def test_1_add_1_eq_2(self):
        # Assert 1 add 1 equals 2
        self.assertEqual(add(1,1), 2)
