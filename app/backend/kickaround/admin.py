from django.contrib import admin

# Register your models here.

from .models import (KickAround,
                        KickAroundSide,
                        KickAroundMember,
                        KickAroundGame,
                        KickAroundGameMember,
                        KickAroundPayment,
                        Location,)


admin.site.register(Location)
admin.site.register(KickAround)
admin.site.register(KickAroundSide)
admin.site.register(KickAroundMember)
admin.site.register(KickAroundGame)
admin.site.register(KickAroundGameMember)
admin.site.register(KickAroundPayment)
