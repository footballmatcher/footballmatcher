import inspect
from enum import Enum

# Enables choices to be selected by dot operater rather than the hardcoded value
class ChoiceEnum(Enum):

    @classmethod
    def choices(cls):
        # get all members of the class
        members = inspect.getmembers(cls, lambda m: not(inspect.isroutine(m)))
        # filter down to just properties
        props = [m for m in members if not(m[0][:2] == '__')]
        # format into django choice tuple
        choices = tuple([(str(p[1].value), p[0]) for p in props])
        return choices

class StandardTypes(ChoiceEnum):
    everyone = 0
    semi_competitive = 1
    competitive = 2

class RolesTypes(ChoiceEnum):
    player = 0
    # Additional organiser who isn't actively involved in organising next weeks game
    passive_organiser = 1
    # The organiser who is actively involved in organising next weeks game
    active_organiser = 2

class GameMemberStatusTypes(ChoiceEnum):
    requested = 0
    pending = 1
    invited = 2
    accepted = 3
    rejected = 4
    paid = 5

class KAPrivacyTypes(ChoiceEnum):
    # Anyone can request to join kickaround
    open = 0
    # Only the organisers and players can invite to join KickAround
    semi_open = 1
    # Only the organiser can invite
    closed = 2

class GamePrivacyTypes(ChoiceEnum):
    # If game is short of numbers -  anyone can request to join this weeks game
    open = 0
    # If game is short of numbers - organisers and players can invite to join this weeks game
    semi_open = 1
    # Only the organiser can invite people to join game
    closed = 2

class GameStatusTypes(ChoiceEnum):
    created = 0
    pending_responses = 1
    game_confirmed = 2
    game_cancelled = 3
    game_played = 4

class MemberStatusTypes(ChoiceEnum):
    non_registered = 0
    pending = 1
    approved = 2
    rejected = 3
    deactivated = 4

class PaymentStatusTypes(ChoiceEnum):
    created = 0
    requested = 1
    paid = 2

class PaymentTypes(ChoiceEnum):
    cash = 0
    paypal = 1

class CurrencyTypes(ChoiceEnum):
    # British Pounds
    GBP = 0
    # US Dollars
    USD = 1

class DayOfWeekTypes(ChoiceEnum):
    monday = 0
    tuesday = 1
    wednesday = 2
    thursday = 3
    friday = 4
    saturday = 5
    sunday = 6
