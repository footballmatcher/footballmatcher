# from django.forms import widgets
from rest_framework import serializers
from profiles.models import Profile


class ProfileSerializer(serializers.ModelSerializer):
    username = serializers.CharField(source='user.username')
    email = serializers.CharField(source='user.email')
    password = serializers.CharField(source='user.password')
    sex = serializers.Field(source='sex')
#     mobile = serializers.Field(source='mobile')
#     has_paypal = serializers.Field(source='has_paypal')
#     paypal_email = serializers.Field(source='paypal_email')
#     profile_pic = serializers.Field(source='profile_pic')

    class Meta:
        model = Profile
        fields = ('user_id', 'username', 'email', 'password', 'sex', 'mobile','has_paypal','paypal_email','profile_pic')

    def restore_object(self, attrs, instance=None):
        """
        Create or update a new snippet instance, given a dictionary
        of deserialized field values.

        Note that if we don't define this method, then deserializing
        data will simply return a dictionary of items.
        """
        if instance is not None:
            instance.user.username   = attrs.get('user.username', instance.user.username)
            instance.user.email      = attrs.get('user.email', instance.user.email)
            instance.user.password   = attrs.get('user.password', instance.user.password)
            instance.sex             = attrs.get('sex', instance.sex)
#             instance.mobile          = attrs.get('mobile', instance.user.mobile)
#             instance.has_paypal      = attrs.get('has_paypal', instance.has_paypal)
#             instance.paypal_email    = attrs.get('paypal_email', instance.paypal_email)
#             instance.profile_pic     = attrs.get('profile_pic', instance.profile_pic)
            return instance

        user = User.objects.create_user(username=attrs.get('user.username'), email= attrs.get('user.email'), password=attrs.get('user.password'))
        return AppUser(user=user)