from django.conf.urls import patterns, url
from kickarounds import views

urlpatterns = [
    url(r'^$', views.KickAroundList.as_view()),
    url(r'^(?P<pk>[0-9]+)$', views.KickAroundDetail.as_view()),
]