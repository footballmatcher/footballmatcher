from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone

#Create your models here.
class KickAround(models.Model):
    name = models.CharField(max_length=200)
    members = models.ManyToManyField(User, through='KickAroundMembership')
    frequency = models.CharField(max_length=200)
    address1  = models.CharField(max_length=200)
    address2  = models.CharField(max_length=200)
    town_city = models.CharField(max_length=200)
    postcode  = models.CharField(max_length=200)
    avg_pitch_cost  = models.CharField(max_length=200)
    avg_player_cost = models.CharField(max_length=200)

class KickAroundSetting(models.Model):
    kickaround = models.OneToOneField(KickAround, primary_key=True)
    pub_date = models.DateTimeField(default=timezone.now())
    public = models.BooleanField(default=False)
    open_ext_players  = models.BooleanField(default=False)
    players_can_invite = models.BooleanField(default=False)
    automate_invites = models.BooleanField(default=False)
    num_days_b4_send_invites = models.IntegerField(default=0)
    automate_payment = models.BooleanField(default=False)
    collect_payment = models.BooleanField(default=False)
    enforce_pay_b4_play = models.BooleanField(default=False)
    chase_payments = models.BooleanField(default=False)
    chase_due_payments_freq = models.CharField(max_length=200)
    accept_cash_payments = models.CharField(max_length=200)
    contact_string = models.TextField() 

class KickAroundMembership(models.Model):
    user        = models.ForeignKey(User) 
    kickaround  = models.ForeignKey(KickAround)
    date_joined = models.DateField(default=timezone.now())
    role        = models.CharField(max_length=200, default="player")
    status      = models.CharField(max_length=200, default="invited")

class KickAroundInvite(models.Model):
    kickaround      = models.ForeignKey(KickAround)
    sender          = models.ForeignKey(User, blank=True, null=True, on_delete=models.SET_NULL, related_name='kickaround_invite_sender') 
    receiver        = models.ForeignKey(User, blank=True, null=True, on_delete=models.SET_NULL, related_name='kickaround_invite_receiver') 
    date_sent       = models.DateField(default=timezone.now())
    unique_url      = models.CharField(max_length=200)
    message         = models.CharField(max_length=200, default="mesage")

class KickGame(models.Model):
    kickaround        = models.ForeignKey(KickAround)
    members           = models.ManyToManyField(User, through='KickGameMembership')
    total_cost        = models.CharField(max_length=200)
    player_cost       = models.CharField(max_length=200)
    game_date_time    = models.DateTimeField('date published')
    game_status       = models.CharField(max_length=200)

class KickGameMembership(models.Model):
    user            = models.ForeignKey(User) 
    kickgame        = models.ForeignKey(KickGame)
    date_invited    = models.DateField(default=timezone.now())
    date_confirmed  = models.DateField(default=timezone.now())
    player_game_status = models.CharField(max_length=200, default="invited")

class KickGamePayment(models.Model):
    kickgame        = models.ForeignKey(KickGame)
    payee           = models.ForeignKey(User, blank=True, null=True, on_delete=models.SET_NULL, related_name='kickgame_payee') 
    payer           = models.ForeignKey(User, blank=True, null=True, on_delete=models.SET_NULL, related_name='kickgame_payer') 
    date_created    = models.DateField(default=timezone.now())
    date_paid       = models.DateField(default=timezone.now())
    payment_type    = models.CharField(max_length=200, default="paypal")
    payment_status  = models.CharField(max_length=200, default="not paid")

class KickGameInvite(models.Model):
    kickgame        = models.ForeignKey(KickGame)
    sender          = models.ForeignKey(User, blank=True, null=True, on_delete=models.SET_NULL, related_name='kickgame_invite_sendersender') 
    receiver        = models.ForeignKey(User, blank=True, null=True, on_delete=models.SET_NULL, related_name='kickgame_invite_receiver') 
    date_sent       = models.DateField(default=timezone.now())
    unique_url      = models.CharField(max_length=200)
    message         = models.CharField(max_length=200, default="mesage")

    
# class KickInviteEmail(models.Model):
#     kickgame          = models.ForeignKey(KickGame)
#     message           = models.TextField()
#     unique_url        = models.TextField()
#     invite_date_time  = models.DateTimeField('date published')
#     organiser         = models.ForeignKey(User)
#     player            = models.ForeignKey(User)

