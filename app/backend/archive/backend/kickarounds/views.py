from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from kickarounds.models import KickAround, KickAroundSetting, KickAroundMembership, KickAroundInvite, KickGame, KickGameMembership, KickGamePayment, KickGameInvite
from kickarounds.serializers import KickAroundSerializer
from rest_framework import generics
from rest_framework import permissions

class KickAroundList(generics.ListCreateAPIView):
    permission_classes = (permissions.AllowAny,)
    queryset = KickAround.objects.all()
    serializer_class = KickAroundSerializer

class KickAroundDetail(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = (permissions.IsAuthenticated,)
    queryset = KickAround.objects.all()
    serializer_class = KickAroundSerializer 
 
 
 
 
# @api_view(['GET'])
# def kickaround_list(request):
#     permission_classes = (permissions.AllowAny,)
#     """
#     List all snippets, or create a new kickaround.
#     """
#     if request.method == 'GET':
#         kickarounds = KickAround.objects.all()
#         serializer = KickAroundSerializer(kickarounds, many=True)
#         return Response(serializer.data)
#      
# @api_view(['GET', 'PUT', 'DELETE'])
# def kickaround_detail(request, pk):
#     """
#     Retrieve, update or delete a kickaround instance.
#     """
#     permission_classes = (permissions.IsAuthenticated,)
#     try:
#         kickaround = KickAround.objects.get(pk=pk)
#     except KickAround.DoesNotExist:
#         return Response(status=status.HTTP_404_NOT_FOUND)
#  
#     if request.method == 'GET':
#         serializer = KickAroundSerializer(kickaround)
#         return Response(serializer.data)
#  
#     elif request.method == 'PUT':
#         serializer = KickAroundSerializer(kickaround, data=request.DATA)
#         if serializer.is_valid():
#             serializer.save()
#             return Response(serializer.data)
#         return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
#  
#     elif request.method == 'DELETE':
#         kickaround.delete()
#         return Response(status=status.HTTP_204_NO_CONTENT)