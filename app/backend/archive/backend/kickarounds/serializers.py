# from django.forms import widgets
from rest_framework import serializers
from profiles.models import Profile
from kickarounds.models import KickAround, KickAroundSetting, KickAroundMembership, KickAroundInvite, KickGame, KickGameMembership, KickGamePayment, KickGameInvite
from users.serializers import UserSerializer

class KickAroundSerializer(serializers.ModelSerializer):
    members = UserSerializer(many=True)

    class Meta:
        model = KickAround
        fields = ('name', 'members', 'frequency', 'address1','address2','town_city','postcode','avg_pitch_cost','avg_player_cost')
        