from django.contrib.auth.models import User
from profiles.models import Profile, Place, Restaurant
from kickarounds.models import KickAround, KickAroundSetting, KickAroundMembership, KickAroundInvite, KickGame, KickGameMembership, KickGamePayment, KickGameInvite
from kickarounds.serializers import KickAroundSerializer
from profiles.serializers import ProfileSerializer

usr  = User(username ="test05", password = "test05")
prof = Profile("sex=m", user = usr)

usr.profile = prof

serializer = ProfileSerializer(prof)

ka = KickAround (name="Thursday night SSW")