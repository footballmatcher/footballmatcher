/**
 * Created by Phil on 25/11/2014.
 */
myApp.controller('DropdownCtrl', function ($scope, $log) {
    $scope.status = {
        isopen: false
    };

    $scope.toggled = function(open) {
        $log.log('Dropdown is now: ', open);
    };

    $scope.toggleDropdown = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.status.isopen = !$scope.status.isopen;
    };
});

myApp.controller('LogInController', function($scope, $rootScope, $http, $state, $log, Auth) {
    // Get access token
    $scope.authenticate = function() {
        Auth.getAccessToken($scope.username, $scope.password);
    };

    $scope.logOut = function() {
        Auth.logout();
    };

});

myApp.controller('NavbarController', function($scope, $rootScope, $location, $state) {

    $scope.isActive = function (route) {
        return route === $location.path();
    };
});

myApp.controller('KickAroundController', function($scope, $rootScope, Kickarounds) {
    $scope.kickarounds = Kickarounds.getKickAroundList();
});

myApp.controller('CookieController', function($scope,$state, $rootScope, $cookieStore) {

    $scope.addOne = function () {
        var lastVal = $cookieStore.get('lastValue');
        if (!lastVal) {
            $scope.cookie_data = 1;
        } else {
            $scope.cookie_data = lastVal + 1;
        }
        $scope.scopeVal = $scope.cookie_data;
    }

    $scope.save = function () {
        $cookieStore.put('lastValue', $scope.cookie_data);
        $scope.scopeVal = "Cookie Saved";
    }

    $scope.remove = function () {
        $cookieStore.remove('lastValue');
        $scope.scopeVal = "Cookie Removed";
    };



});
