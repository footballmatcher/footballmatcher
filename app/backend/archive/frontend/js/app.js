var myApp;
myApp = angular.module('myApp', ['ngCookies','ui.router', 'ui.bootstrap', 'ngResource']);

myApp.config(function($stateProvider, $locationProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('/home');

    $stateProvider

        // HOME STATES AND NESTED VIEWS ========================================
        .state('home', {
            url: '/home',
            templateUrl: 'partials/p_home.html',
            controller: function ($scope) {
                $scope.number = 5;
            }
        })

        // LOGIN
        .state('login', {
            url: '/login',
            templateUrl: 'partials/p_login.html'
        })

        // LOG OUT
        .state('logout', {
            url: '/logout',
            templateUrl: 'partials/p_logout.html'
        })

        // LOG OUT
        .state('signup', {
            url: '/logout',
            templateUrl: 'partials/p_signup.html'
        })

        // ACCOUNT
        .state('account', {
            url: '/account',
            templateUrl: 'partials/p_account.html'
        })

        // PROFILE
        .state('profile', {
            url: '/profile',
            templateUrl: 'partials/p_profile.html'
        })

        // PAYMENTS
        .state('payments', {
            url: '/payments',
            templateUrl: 'partials/p_payments.html',
            controller: function ($scope) {
                $scope.number = 5555;
            }
        })

        // PAYMENT
        .state('payment', {
            url: '/users/{user_id}/payment/{payment_id}',
            templateUrl: 'partials/p_payment.html',
            controller: function ($scope, $stateParams) {
                $scope.payment_id = $stateParams.payment_id;  //*** Exists! ***//
                $scope.user_id = $stateParams.user_id;  //*** Exists! ***//
            }
        })

        // LANDING
        .state('landing', {
            url: '/landing',
            templateUrl: 'partials/p_landing.html'
        })

        // KICKAROUND CREATE
        .state('ka_create', {
            url: '/kickaround/create',
            templateUrl: 'partials/p_ka_create.html'
        })

        // KICKAROUND FIND
        .state('ka_find', {
            url: '/kickaround/find',
            templateUrl: 'partials/p_ka_find.html'
        });
})


.run(function($rootScope) {
    $rootScope.CONFIG = {
        //apiUrl: 'http://212.227.84.229:8080',
        apiUrl: 'http://footballmatcher.co.uk:8000',
        clientId: 'T;Ousuhi_L!DVFbpWD;hjuDi!QXpsW4aR0lwb!ID',
        clientSecret: '@_.vIRKmdjCjGNCdp51j4ixTTz;r;Q-UOcRaLrfN6Vrvn9KlWCq;pCZPwIRv=zcO166;nD-DZY?KjoBqmnVBWzMl.keQa.hIf@;c6vYXhFyZtwtf.4bQM3Ls@RwBkK.a'
    };
})

.run(function($cookieStore, $location, $http, $rootScope, Auth) {
    Auth.authenticate()
});
