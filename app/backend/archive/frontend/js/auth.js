myApp.factory('Auth', function ($cookieStore, $http, $location, $rootScope, $state, $log, transformRequestAsFormPost) {
    // Service logic
    // ...

    // User Cookie Store
    var auth = $cookieStore.get('auth');

    // Access token
    var accessToken;

    var redirectToLogin = function() {
        return $state.go('signup');
    };
    var redirectToHome = function () {
        return $state.go('home');
    };

    var getAuthToken = function() {
        try {
            return auth.access_token;
        } catch (e) {}
    };

    var authenticate = function() {
        if (typeof(accessToken) == 'undefined') {
            accessToken = getAuthToken();
        }

        // Let's attempt an API call
        $http.defaults.headers.common['Authorization'] = 'Bearer ' + accessToken;
        var config = {
            'method': 'GET',
            'url': $rootScope.CONFIG.apiUrl + '/users/'
        };

        $http(config) // Get user data
        .success(function(data) {
            $rootScope.user = data;
            $rootScope.isLoggedIn = true;
            $log.log("USER DATA");
            $log.log(data);
        })
        .error(function(data, status) {
            if (status == 0) {
                console.log('Could not reach API')
            }
            $rootScope.isLoggedIn = false;
            redirectToLogin();
        });
    }

    // Public API here
    return {
        authenticate: function() {
            return authenticate();
        },

        getExistingAccessToken: function() {
            return getAuthToken();
        },

        getAccessToken: function(username, password) {
            $log.log(username);
            $log.log(password);

            var data =  {
                    client_id: $rootScope.CONFIG.clientId,
                    client_secret: $rootScope.CONFIG.clientSecret,
                    grant_type: 'password',
                    username: username,
                    password: password
                };

            $log.log(data);

            $http({
                method: "POST",
                url: $rootScope.CONFIG.apiUrl + "/o/token/",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                transformRequest: function(obj) {
                    var str = [];
                    for(var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                data: data
            }).success(function(data, status, headers, config) {
                console.log('Authentication Successful!');
                $cookieStore.put('auth', data)
                $rootScope.isLoggedIn = true;
                console.log(data.access_token);
                authenticate(data.access_token);
                redirectToHome();

            }).error(function(data, status) {
                $rootScope.isLoggedIn = false;
                if (status == 0) {
                    console.log('Could not reach API');
                } else if (data.error == 'invalid_grant') {
                    console.log('Invalid username & password');
                }
            });
        },

        logout: function() {
            $rootScope.isLoggedIn = false;
            $cookieStore.remove('auth');
            redirectToLogin();
        },

        redirectToLogin: function() {
            return redirectToLogin();
        },
        redirectToHome: function() {
            return redirectToHome();
        }
    }
});
