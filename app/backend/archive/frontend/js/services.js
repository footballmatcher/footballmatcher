/**
* Created by Phil on 25/11/2014.
*/
myApp.factory('Kickarounds', function ($cookieStore ,$http, $location, $rootScope, $state, $log, transformRequestAsFormPost, Auth) {

    // Access token
    var accessToken;

    var redirectToLogin = function() {
        return $state.go('signup');
    };

    var getKickAroundList = function() {
        var kickarounds = [];

        if (typeof(accessToken) == 'undefined') {
            accessToken = Auth.getExistingAccessToken();
        }

        // Let's attempt an API call
        $http.defaults.headers.common['Authorization'] = 'Bearer ' + accessToken;
        var config = {
            'method': 'GET',
            'url': $rootScope.CONFIG.apiUrl + '/kickarounds/'
        };

        $http(config) // Get user data
            .success(function(data) {
                console.log('Kickarounds have been found');
                console.log(data);
                return data;
            })
            .error(function(data, status) {
                if (status == 0) {
                    console.log('Could not reach API')
                }
                return "Kickaround Error";
                redirectToLogin();
            });
    }


    // Public API here
    return {
        getKickAroundList: function() {
            return getKickAroundList();
        }
    }
});
