import datetime
from kickaround.models import *
from kickaround.tasks.email_tasks import *
from kickaround.enums import *

# Get kickaround
ka = KickAround.objects.get(id=1)
games = ka.kickaround_games.all()
game = games[0]
org = ka.members.filter(kickaroundmember__role = RolesTypes.organiser.value)
member = ka.members.filter(username="Julian")[0]

# Send Kickaround game
send_games_payment_due_emails(ka, game)
#mem_payments = ka.payments.filter(payee=org).filter(payer=member).filter(payment_status=PaymentStatusTypes.created)
send_game_invitation_emails(ka, game)

# # Returns a dict of the current day of the week plus an time range between now and an hours time
# def get_date_time_range():
#     # Get current date time
#     now = datetime.datetime.now()
#     # FIXME - SET TO SPECIFIC KA TIME - MONDAY NIGHT 8PM
#     # Zero the minutes, seconds and hour
#     now = now.replace(month=3, day=2, hour=20,minute=0, second=0, microsecond=0)
#     # Get day of the week
#     day_of_week = DAY_OF_WEEK[now.weekday()]
#     # Get datetime in an hour
#     now_plus_1hr = now + datetime.timedelta(hours=+1)
#     # Get date time of next week
#     next_week = now + datetime.timedelta(days=+7)
#     # Create dictionary of day of week and the time range
#     time_range_dict = {'day_of_week': day_of_week, 'now': now.time(), 'now_plus_1hr': now_plus_1hr.time(), 'next_week':next_week}
#     return time_range_dict
#
# # Get today, the search time range
# time_range_dict = get_date_time_range()
# day_of_week = time_range_dict['day_of_week']
# now = time_range_dict['now']
# now_plus_1hr = time_range_dict['now_plus_1hr']
# next_week_time = time_range_dict['next_week']
#
# print "Get Kickarounds that have games scheduled to start between " + str(now) + " and " + str(now_plus_1hr) + " this " + str(day_of_week[1])
#
# # Get KickArounds that are schedule a week from today within the time range
# kas = KickAround.objects.filter(day_of_week=day_of_week).filter(weekly_time__gte=now, weekly_time__lt=now_plus_1hr)