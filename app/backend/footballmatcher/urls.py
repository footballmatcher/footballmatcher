from django.conf.urls import patterns, include, url
from django.contrib import admin
from rest_framework.routers import DefaultRouter
from kickaround.views import model_views, response_views, task_views
from django.conf import settings

admin.autodiscover()

# Create a router and register our viewsets with it.
router = DefaultRouter()
router.register(r'users', model_views.UserViewSet)
router.register(r'locations', model_views.LocationViewSet)
router.register(r'members', model_views.KickAroundMemberViewSet)
router.register(r'games', model_views.KickAroundGameViewSet)
router.register(r'gamemembers', model_views.KickAroundGameMemberViewSet)
router.register(r'payments', model_views.KickaroundPaymentViewSet)

urlpatterns = patterns('',
    url(r'^', include(router.urls)),

    # Kickaround Model Views
    url(r'^kickarounds_ks/(?P<pk>[0-9]+)/$', model_views.KickAroundKSDetail.as_view()),
    url(r'^kickarounds_ks/$', model_views.KickAroundKSList.as_view()),
    url(r'^kickarounds/$', model_views.KickAroundList.as_view()),
    url(r'^kickarounds/(?P<pk>[0-9]+)/$', model_views.KickAroundDetail.as_view()),

    # # Kickaround Manual Tasks
    url(r'^kickarounds/(?P<kickaround_id>[0-9]+)/tasks/send_join_ka_invites/$', task_views.SendJoinKickaroundInvitationsEmails.as_view()),
    url(r'^kickarounds/(?P<kickaround_id>[0-9]+)/tasks/send_join_ka_invites/status/(?P<task_id>.*)/$', task_views.GetJoinKickaroundInvitationTaskStatus.as_view()),
    url(r'^kickarounds/(?P<kickaround_id>[0-9]+)/tasks/send_game_invites/$', task_views.SendGameInvitationEmails.as_view()),
    url(r'^kickarounds/(?P<kickaround_id>[0-9]+)/tasks/send_game_invites/status/(?P<task_id>.*)/$', task_views.GetGameInvitationTaskStatus.as_view()),
    url(r'^kickarounds/(?P<kickaround_id>[0-9]+)/tasks/confirm_game/$', task_views.SendGameConfirmationEmails.as_view()),
    url(r'^kickarounds/(?P<kickaround_id>[0-9]+)/tasks/confirm_game/status/(?P<task_id>.*)/$', task_views.GetGameConfirmationTaskStatus.as_view()),
    url(r'^kickarounds/(?P<kickaround_id>[0-9]+)/tasks/cancel_game/$', task_views.SendGameCancellationEmails.as_view()),
    url(r'^kickarounds/(?P<kickaround_id>[0-9]+)/tasks/cancel_game/status/(?P<task_id>.*)/$', task_views.GetGameCancellationTaskStatus.as_view()),

    # Respond to notification over email
    url(r'^email_response/join_kickaround/$', response_views.RespondToKickAroundJoinInvitation.as_view()),
    url(r'^email_response/game_invite/$', response_views.RespondToKickAroundGameInvitation.as_view()),
    url(r'^email_response/game_confirmation/$', response_views.RespondToGameConfirmation.as_view()),
    url(r'^email_response/payment_due/$', response_views.RespondToPaymentDue.as_view()),
    url(r'^email_response/request_to_join/$', response_views.RespondToRequestToJoin.as_view()),

    # Authentication Scheme
    url(r'^o/', include('oauth2_provider.urls', namespace='oauth2_provider')),

    # Admin Site Enabled
    url(r'^admin/', include(admin.site.urls)),

    # API Documentation
    url(r'^docs/', include('rest_framework_swagger.urls')),

    # API Manual Authentication
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
)

if settings.DEBUG:
    import debug_toolbar
    urlpatterns += patterns('',
        url(r'^__debug__/', include(debug_toolbar.urls)),
    )
