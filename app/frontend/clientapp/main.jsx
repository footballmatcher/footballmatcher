var React = require('react');
var Router = require('react-router');

var DefaultRoute = Router.DefaultRoute;
var Link = Router.Link;
var Route = Router.Route;
var RouteHandler = Router.RouteHandler;

var App = require('./app/App.jsx');
var Admin = require('./admin/Admin.jsx');
var Kickaround = require('./kickaround/Kickaround.jsx');
var Team = require('./teams/Teams.jsx');
var Landing = require('./landing/Landing.jsx');
var Competition = require('./competitions/Competitions.jsx');
var Home = require('./home/Home.jsx')
var routes = (
    <Route handler={App}  path="/">
      <Route name="landing" handler={Landing}/>
      <Route name="home" handler={Home}/>
      <Route name="kickaround" handler={Kickaround}/>
      <Route name="team" handler={Team}/>
    </Route>
);

Router.run(routes, function (Handler, state) {
    var params = state.params;
    React.render(<Handler params={params}/>, document.getElementById('app'));
});
