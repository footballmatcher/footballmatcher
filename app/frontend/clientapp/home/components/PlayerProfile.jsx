var React = require('react');
var {
        Table
    } = require('react-bootstrap');

var stateTree = require('../../app/stateTree.js');

var profileCursor = stateTree.select('playerProfile');
// if the return data is  a JSOn object say  {name: 'zzz', age: 28}
// it will loop over the object propertieds and display a table of sorts.
var PlayerProfile = React.createClass({

    mixins: [profileCursor.mixin],

    getRowNodes: function(row, i) {
      return (<tr key={i}>
                <td><b>{row[0]}</b></td>
                <td>{row[1]}</td>
            </tr>);
    },

    getPropertiesNodes: function(obj) {

        var data = [];
        for (var key in obj) {
            data.push([key, obj[key]]);
        }

        var nodes = data.map((x, i) => this.getRowNodes(x, i));
        return (nodes);
    },

    render: function() {
        var playerProfile = this.state.cursor;

        return (
            <Table responsive striped bordered condensed hover>
                <tbody>
                    {this.getPropertiesNodes(playerProfile)}
                </tbody>
            </Table>
        );
    }
});

module.exports = PlayerProfile;