var React = require('react');
var {
        Button, Input, Col, Row
    } = require('react-bootstrap');

var PlayerProfile = require('./components/PlayerProfile.jsx');
var actions = require('./actions');
var stateTree = require('../app/stateTree');

var Home = React.createClass({
    mixins: [stateTree.mixin, React.addons.LinkedStateMixin],

    cursors: {
        profile: ['playerProfile'],
    },

    getInitialState: function() {
        return {
            uid: '',
            pwd: '',
        }
    },

    handleClick: function() {
        console.log("Button Pressed");
        actions.getToken(this.state.uid, this.state.pwd);
        console.log(this.state);
    },

    render: function() {

        return (
            <Row>
                <Col md={2}>
                <form>
                    <Input type="text" label="User Id" placeholder="Enter Id" valueLink={this.linkState('uid')}/>
                    <Input type='password' label='Password' placeholder="Enter Pwd" valueLink={this.linkState('pwd')}/>
                    <Button type="submit" onClick={this.handleClick}>Login</Button>
                </form>
                </Col>
                <Col md={4}>
                    <h3> Player Profile </h3>
                    <p> {stateTree.get('playerProfile')}</p>
                </Col>
            </Row>
        );
    }
});

module.exports = Home;