var React = require('react');
var {
        Button, Input, Col, Row
    } = require('react-bootstrap');

var PlayerProfile = require('./components/PlayerProfile.jsx');
var actions = require('./actions');
var stateTree = require('../app/stateTree');

var Competitions = React.createClass({
    mixins: [stateTree.mixin, React.addons.LinkedStateMixin],

    cursors: {
        profile: ['playerProfile'],
    },

    getInitialState: function() {
        return {
            uid: '',
            pwd: '',
        }
    },

    handleClick: function() {
        console.log("Button Pressed");
        actions.getToken(this.state.uid, this.state.pwd);
        console.log(this.state);
    },

    render: function() {

        return (
            <Row>
                <h3> Competitions Page </h3>
            </Row>
        );
    }
});

module.exports = Competitions;