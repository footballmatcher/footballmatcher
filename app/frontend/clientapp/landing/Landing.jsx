var React = require('react');
var {
        Button, Input, Col, Row, Grid, Panel, Jumbotron
    } = require('react-bootstrap');

const title = (
    <h3>Panel title</h3>
);


var Landing = React.createClass({
    render: function() {

        return (
            <Grid>
                <Row>
                    <Jumbotron>
                        <h1>Welcome to Football Matcher</h1>
                        <p>This is a simple hero unit, a simple jumbotron-style component for calling extra attention to featured content or information.</p>
                        <p><Button bsStyle='primary'>Learn more</Button></p>
                    </Jumbotron>
                    <div>
                        <Panel header={title}>
                            Panel content
                        </Panel>
                    </div>
                </Row>
            </Grid>
        );
    }
});

module.exports = Landing;