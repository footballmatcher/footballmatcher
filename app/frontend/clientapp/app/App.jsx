var React = require('react');
var Router = require('react-router');
var Link = Router.Link;
var RouteHandler = Router.RouteHandler;
var FMNavBar =  require('../fmnavbar/FMNavBar.jsx');

var App = React.createClass({
  render: function () {
    return (
      <div>
        <header>
          <FMNavBar> </FMNavBar>
        </header>

        {/* this is the important part */}
        <RouteHandler {...this.props}/>
      </div>
    );
  }
});

module.exports = App;

