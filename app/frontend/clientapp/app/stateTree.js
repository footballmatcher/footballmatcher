var Baobab = require('baobab');
var ReactAddons = require('react/addons');


appState = {
    playerProfile: {}
};

stateOptions = {
    mixins: [ReactAddons.PureRenderMixin],
    shiftReferences: true
};

var stateTree = new Baobab(appState, stateOptions);

module.exports = stateTree;