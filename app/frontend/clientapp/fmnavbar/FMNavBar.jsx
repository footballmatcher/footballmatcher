React = require('react');
var {Navbar, Nav} = require('react-bootstrap');
var {NavItemLink} = require('react-router-bootstrap')

var FMNavBar = React.createClass({

    getInitialState: function() {
        return {
            loggedIn: false
        }
    },

    render: function() {
        return (
            <Navbar brand="Football Matcher" inverse toggleNavKey={0}>
                <Nav left eventKey={0}> {/* This is the eventKey referenced */}
                    <NavItemLink eventKey={1} to="landing">Landing</NavItemLink>
                    {
                        this.state.loggedIn && <NavItemLink eventKey={2} to="home">Kickaround</NavItemLink>
                    }
                    {
                        this.state.loggedIn && <NavItemLink eventKey={3} to="kickaround">Kickaround</NavItemLink>
                    }
                    {
                        this.state.loggedIn && <NavItemLink eventKey={4} to="team">Teams</NavItemLink>
                    }
                    {
                        this.state.loggedIn && <NavItemLink eventKey={5} to="team">Competitions</NavItemLink>
                    }
                    {
                        this.state.loggedIn &&
                        <NavItemLink eventKey={5} to="team">Settings</NavItemLink>
                    }
                </Nav>
            </Navbar>
        );
    }
});

module.exports = FMNavBar;