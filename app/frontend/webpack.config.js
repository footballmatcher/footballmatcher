/**
 * Created by gokulnath.haribabu on 22/05/2015.
 */

var path = require('path');
var webpack = require('webpack');
var appPath = path.resolve(__dirname, 'clientapp');
var node_modules_dir = path.resolve(__dirname, 'node_modules');
var ExtractTextPlugin = require('extract-text-webpack-plugin')

var config = {
  entry: path.resolve(appPath, 'main.jsx'),
  devtool: 'eval',
  output: {
    path: path.resolve(__dirname, 'static/clientapp'),
    filename: 'bundle.js',
  },
  module: {
    loaders: [
      { test: /\.jsx$/, loader: 'jsx-loader?harmony'},
      { test: /\.css$/, loader: ExtractTextPlugin.extract('style-loader', 'css-loader') },
      { test: /\.less$/, loader: 'style-loader!css-loader!less-loader' }, // use ! to chain loaders
      { test: /\.(otf|eot|jpg|png|svg|ttf|woff|woff2)(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: 'url-loader?limit=8192' }, // inline base64 URLs for <=8k images, direct URLs for the rest
      { test: /\.json$/, loader: 'json'}
    ]
  },
 plugins: [
    new webpack.OldWatchingPlugin(),
    new ExtractTextPlugin('bundle.css')
  ]
};


module.exports = config;