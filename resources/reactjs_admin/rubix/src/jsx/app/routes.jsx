/* ERROR PAGES */
var notfound = require('./routes/notfound.jsx');

/* APP PAGES */
var blank = require('./routes/app/blank.jsx');
var homepage = require('./routes/app/homepage.jsx');

/* ROUTES */
module.exports = (
  <Route handler={ReactRouter.RouteHandler}>
    <DefaultRoute handler={homepage} />
    <Route path='/' handler={homepage} />
    <NotFoundRoute handler={notfound} />
  </Route>
);
